<Class name="DocumentHub" id="5010879c-910d-49ce-a04b-3ad7b29d53e5">
    <superclass name="Object"/>
    <persistentAllowed>true</persistentAllowed>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>The name=" provides basic documentation services to other schemas.

To provide this service, the schema exports a Package that includes the helper class
DocumentHub, and the Jade interface DocumentInterface.

Any other schema that wants to make use of the documentation service needs to
import the package.

The importing schema can make use of the documentation features via very unobtrusive
means. Any class that is to participate in the documentation service needs to implement
the imported interface (DocumentInterface). Implementors of the interface can add or
remove themselves from a (conceptual) document set by calling the methods that have 
been exported by DocumentHub (the method parameters of the exported methods are of 
type DocumentInterface). Documentation details are shown to the user via a form that
resides in the exporting schema.

Note that exposing a Jade interface via a package has resulted in an extremely flexible 
mechanism to provide services between unrelated schemas. The exporting schema provides
exported methods that accept parameters of type DocumentInterface. It doesn't care how
the implementing schema decides to use the facility, the only proviso is that the
participants implement the DocumentInterface. Similarly, the importing schema does not
need to know about the underlying storage mechanism used to collate the items that are
to be documented, or how the display form works,  - it is only interested in deciding 
which classes should implement the DocumentInterface interface so that they are 
eligible to use the service.</text>
    <DbClassMap file="selfdocmap"/>
    <ImplicitInverseRef name="callersAppContext" id="f260d754-bd75-4021-b183-d0995e222cd2">
        <embedded>true</embedded>
        <type name="ApplicationContext"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="documentSet" id="7bce8b24-547d-4dad-988f-43e3d825bb07">
        <memberTypeInverse>true</memberTypeInverse>
        <type name="SelfDocumentorSet"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <JadeMethod name="addDocumentationObject" id="3aff9a2c-6151-4256-aa7c-17493cd882d2">
        <source>addDocumentationObject(object : DocumentInterface);

// --------------------------------------------------------------------------------
// Method:		addDocumentationObject
//
// Purpose:		Adds an object that implements the DocumentInterface interface to a
//				set of items that will (at some future stage) be asked to document
//				themselves.
//
// Parameters:	The object that is to be documented
// --------------------------------------------------------------------------------
vars

begin
	if object = null then
		return;
	endif;
	
	// If the item is already in the set, skip it
	on Exception do zCollAddExceptionHandler(exception);

	documentSet.add(object);
end;
</source>
        <Parameter name="object">
            <type name="DocumentInterface"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="clearDocumentationObjects" id="21e3095a-10a8-4814-afd1-a55a41c50d84">
        <updating>true</updating>
        <source>clearDocumentationObjects() updating;

// --------------------------------------------------------------------------------
// Method:		clearDocumentationObject
//
// Purpose:		Clears the current set of documentation object.
//
// Parameters:	none
// --------------------------------------------------------------------------------
vars

begin
	documentSet.clear;
end;
</source>
    </JadeMethod>
    <JadeMethod name="displayDocumentationDetails" id="38b85476-b650-49e0-bc46-c8e445e7bf95">
        <source>displayDocumentationDetails();

// --------------------------------------------------------------------------------
// Method:		displayDocumentationDetails
//
// Purpose:		Show the form that will display the documentation details for
//				the items that have been added to the documentSet.
//
// Parameters:	none
// --------------------------------------------------------------------------------

vars
	form : DisplaySelfDocumentorDetails;
begin
	create form transient;
	form.myHub := self;
	form.show;
end;
</source>
    </JadeMethod>
    <JadeMethod name="removeDocumentationObject" id="2034b5a2-f3e6-4ecc-b2ce-eef6a60a4efe">
        <source>removeDocumentationObject(object : DocumentInterface);

// --------------------------------------------------------------------------------
// Method:		removeDocumentationObject
//
// Purpose:		Removes an an object from the documentSet.
//
// Parameters:	The object that is to be removed
// --------------------------------------------------------------------------------
vars

begin
	if documentSet.includes(object) then
		documentSet.remove(object);
	endif;	
end;
</source>
        <Parameter name="object">
            <type name="DocumentInterface"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="setCallersApplicationContext" id="d920d205-cb2f-4f85-8285-2427da9760b9">
        <updating>true</updating>
        <source>setCallersApplicationContext(context : ApplicationContext)updating;

// --------------------------------------------------------------------------------
// Method:		setCallersApplicationContext
//
// Purpose:		hold a reference to the callers ApplicationContext. This will be used
//				when performing callback routines to the importing schema. Usage is
//				demonstrated in DisplaySelfDocumentorDetails.displaySelected()
//
// Parameters:	context - the callers ApplicationContext (appContext)
// --------------------------------------------------------------------------------

vars

begin
	self.callersAppContext := context;
end;
</source>
        <Parameter name="context">
            <type name="ApplicationContext"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="zCollAddExceptionHandler" id="5b84bfbd-4b4f-4aa1-84d4-a1c67b06c418">
        <source>zCollAddExceptionHandler(exObj : Exception) : Integer protected;
// --------------------------------------------------------------------------------
// Method:		zCollAddExceptionHandler
//
// Purpose:		Exception handler that can be used to skip additions of objects to
//              a collection where the object already exists in the collection.
//              Arming an exception handler to catch any occurrences of this is
//              more efficient than guarding every "add" with an "includes" call.
//              Of course, this handler should only be armed in cases where it is
//              valid to have an object already in the collection.
// --------------------------------------------------------------------------------
constants
    ObjectAlreadyInCollection = 1309;

begin
    if exObj.errorCode = ObjectAlreadyInCollection then
    	// The object already exists in the collection so resume to the next
    	// statement in the method that armed the handler
        return Ex_Resume_Next;
    endif;
    // Some other exception, so pass it back to the next handler
    return Ex_Pass_Back;
end;</source>
        <access>protected</access>
        <Parameter name="exObj">
            <type name="Exception"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
</Class>
