<JadeInterface name="DocumentInterface" id="75d91ea0-2f1d-4db5-99b1-1950ec89aa42">
    <text>The name=" provides basic documentation services to other schemas.

To provide this service, the schema exports a Package that includes the helper class
DocumentHub, and the Jade interface DocumentInterface.

Any other schema that wants to make use of the documentation service needs to
import the package.

The importing schema can make use of the documentation features via very unobtrusive
means. Any class that is to participate in the documentation service needs to implement
the imported interface (DocumentInterface). Implementors of the interface can add or
remove themselves from a (conceptual) document set by calling the methods that have 
been exported by DocumentHub (the method parameters of the exported methods are of 
type DocumentInterface). Documentation details are shown to the user via a form that
resides in the exporting schema.

Note that exposing a Jade interface via a package has resulted in an extremely flexible 
mechanism to provide services between unrelated schemas. The exporting schema provides
exported methods that accept parameters of type DocumentInterface. It doesn't care how
the implementing schema decides to use the facility, the only proviso is that the
participants implement the DocumentInterface. Similarly, the importing schema does not
need to know about the underlying storage mechanism used to collate the items that are
to be documented, or how the display form works,  - it is only interested in deciding 
which classes should implement the DocumentInterface interface so that they are 
eligible to use the service.</text>
    <JadeInterfaceMethod name="documentSelf" id="be8b519d-22a0-46ee-8c75-7e7e4fb358e2">
        <source>documentSelf():String;</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeInterfaceMethod>
    <JadeInterfaceMethod name="documentSelfRTF" id="5f6e0a97-698d-4edc-bde6-a8a0588e9f33">
        <source>documentSelfRTF():String;</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeInterfaceMethod>
    <JadeInterfaceMethod name="listEntryDescription" id="b04bf9eb-63c6-477f-9b6c-a7cae693c95d">
        <source>listEntryDescription():String;</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeInterfaceMethod>
</JadeInterface>
