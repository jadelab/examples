<GUIClass name="DisplaySelfDocumentorDetails" id="7c59e6c7-2df0-438c-8226-e2d7298dd3ca">
    <superclass name="Form"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <ImplicitInverseRef name="lblDocRTF" id="bd821419-116f-42fe-b5e3-77723e8ace5b">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lblDocText" id="4fa4a558-947d-4ce7-98fb-6cdf2bd4f284">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lblSelected" id="e7dba3c5-ac2c-4548-8c88-bbe1702c9b7f">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lstAvailable" id="14da0a01-38f1-4f05-8988-8d7e391bd070">
        <embedded>true</embedded>
        <type name="ListBox"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="myHub" id="0bdd4119-4b6c-417e-a238-3e78d97eeab9">
        <embedded>true</embedded>
        <type name="DocumentHub"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="rtfDetails" id="1c3de525-3253-467f-96dc-ac8c98485a99">
        <embedded>true</embedded>
        <type name="JadeRichText"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="txtDetails" id="06175b42-fafb-4678-899d-5a39e0202762">
        <embedded>true</embedded>
        <type name="TextBox"/>
    </ImplicitInverseRef>
    <JadeMethod name="displaySelected" id="33be77ce-bfd2-4a44-b0b5-960de563e851">
        <source>displaySelected(obj : Object);

// --------------------------------------------------------------------------------
// Method:		displaySelected
//
// Purpose:		display details of an item that responds to the DocumentInterface.
//
//				an item that has been 'registered' by the importing schema is asked
//				to display its documentation details. This form works with the 
//				DocumentInterface interface and knows nothing about how the interface
//				methods have been implemented in the importing schema. Likewise the
//				importing schema needs to know nothing about the workings of this form.
//
// Parameters:	obj - the object that is to be documented
// --------------------------------------------------------------------------------

vars
	docitem : DocumentInterface;
	context	: ApplicationContext;
begin
	//clear the result text fields
	txtDetails.text 	:= null;
	rtfDetails.textRTF	:= null;
	
	//return if no object passed or the object does not respond to the required interface
	if obj = null or not obj.respondsTo(DocumentInterface) then
		return;
	endif;	

	//we can cast the object back to the interface
	docitem := obj.DocumentInterface;
	
	//we need to specify the application context that we call back on
	context := myHub.callersAppContext;
	
	//ask the registered item to document itself
	txtDetails.text 	:= docitem.documentSelf() in context;
	rtfDetails.textRTF	:= docitem.documentSelfRTF() in context;
end;</source>
        <Parameter name="obj">
            <type name="Object"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="load" id="0b0c1eec-6550-41e1-81b0-6b883c787038">
        <controlMethod name="Form::load"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>load() updating;

vars

begin
	//initialize the form with the current entries in the document hubs list
	loadItems();
end;
</source>
    </JadeMethod>
    <JadeMethod name="loadItems" id="cc2e5643-fd0b-45aa-a4ca-9062d72bdf6c">
        <source>loadItems();

// --------------------------------------------------------------------------------
// Method:		loadItems
//
// Purpose:		display items that have been 'registered' by the importing schema as
//				being candidates for this schemas exposed documentation service.
//
// Parameters:	none
// --------------------------------------------------------------------------------


vars
	docitem : DocumentInterface;
begin
	//clear the list
	lstAvailable.clear;

	//list each of the items in the documentSet.
	//note that we use the exported interfaces method listEntryDescription() to provide the details
	//of what string to put into the list.
	foreach docitem in myHub.documentSet do
		lstAvailable.itemObject[lstAvailable.addItem(docitem.listEntryDescription())] := docitem.Object;
	endforeach;
end;
</source>
    </JadeMethod>
    <JadeMethod name="lstAvailable_click" id="c791f509-bc9f-4e90-bc18-9a784e472092">
        <controlMethod name="ListBox::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>lstAvailable_click(listbox: ListBox input) updating;

vars
	obj		: Object;
begin
	//retrieve the item from the listbox
	obj := listbox.itemObject[listbox.listIndex];
	
	//display details for selected item
	displaySelected(obj);
end;
</source>
        <Parameter name="listbox">
            <usage>input</usage>
            <type name="ListBox"/>
        </Parameter>
    </JadeMethod>
</GUIClass>
