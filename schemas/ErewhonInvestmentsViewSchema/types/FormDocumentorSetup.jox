<GUIClass name="FormDocumentorSetup" id="cc38f62d-8c8f-4c48-a0e9-3f97d2030f7c">
    <superclass name="FormMdiChild"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>This form shows a very simple example of using a JadeInterface to group and report 
over different parts of the Erewhon model.

In the ErewhonInvestmentsModelSchema a JadeInterface has been created. The interface 
is called SelfDocumentor and contains one method (documentSelf).

For a class to implement an interface, the class MUST provide a mapping for each of 
the methods exposed by the interface. 

In this example, three separate classes have implemented the SelfDocumentor interface.
The only requirement for the mapping method is that it has a compatable signature with
that of the interface method (ie, the method name is not pre-determined). This provides
a great level of flexibility for HOW a class implements an interface. SaleItem has
mapped an existing method (getDebugString) to the interface method, AddressableEntity
has added a new method (getDocumentString), and ActivityAgent has added a new method 
(documentSelf) to satisfy the interface implementation requirements.

This form shows how the three unrelated classes can be grouped and worked with in a 
type safe manner (in this example, instances of the classes are held in a set that
has a membership of SelfDocumentor, then the set is iterated and requests that each
member 'documents' itself). 

</text>
    <ImplicitInverseRef name="btnShow" id="7b0d5901-699d-4697-97ca-cb479cc701fa">
        <embedded>true</embedded>
        <type name="Button"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lblSelectItems" id="9b134751-1282-4093-819d-024f4b18afd0">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lstCandidates" id="b283cde6-24d8-49b3-880a-afad27f89178">
        <embedded>true</embedded>
        <type name="ListBox"/>
    </ImplicitInverseRef>
    <JadeMethod name="btnShow_click" id="bde22506-b063-4aee-9a66-557f3fd23064">
        <controlMethod name="Button::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>btnShow_click(btn: Button input) updating;

vars

begin
	app.myDocumentHub.displayDocumentationDetails();
end;
</source>
        <Parameter name="btn">
            <usage>input</usage>
            <type name="Button"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="lstCandidates_click" id="9ce62da7-86dc-4238-9f9c-bbbaeb91c830">
        <controlMethod name="ListBox::click"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>lstCandidates_click(listbox: ListBox input) updating;

vars
	selected 	: Boolean;
	listItem	: Object;
	docitem		: DocumentInterface;
begin
	listItem := listbox.itemObject[listbox.listIndex];
	
	//test that the item responds to the interface
	if listItem &lt;&gt; null and listItem.respondsTo(DocumentInterface) then
	
		//cast the list object back to the DocumentInterface
		docitem  := listItem.DocumentInterface;

		selected := listbox.itemSelected[listbox.listIndex];

		//add or remove the item from the documentation required list
		if selected then
			app.myDocumentHub.addDocumentationObject(docitem);
		else
			app.myDocumentHub.removeDocumentationObject(docitem);
		endif;
	endif;
end;
</source>
        <Parameter name="listbox">
            <usage>input</usage>
            <type name="ListBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="lstCandidates_dblClick" id="7c0acbcf-9ab0-4f7c-a8ca-ab88e59c3923">
        <controlMethod name="ListBox::dblClick"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>lstCandidates_dblClick(listbox: ListBox input) updating;

vars

begin
	listbox.itemExpanded[listbox.listIndex] := not listbox.itemExpanded[listbox.listIndex];
end;
</source>
        <Parameter name="listbox">
            <usage>input</usage>
            <type name="ListBox"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="lstCandidates_pictureClick" id="08837fb8-6bee-4079-b8dc-34e5dc2f66a8">
        <controlMethod name="ListBox::pictureClick"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>lstCandidates_pictureClick(listbox: ListBox input; picIndex: Integer; whatClicked: Integer) updating;

vars

begin
	listbox.itemExpanded[picIndex] := not listbox.itemExpanded[picIndex];
end;
</source>
        <Parameter name="listbox">
            <usage>input</usage>
            <type name="ListBox"/>
        </Parameter>
        <Parameter name="picIndex">
            <type name="Integer"/>
        </Parameter>
        <Parameter name="whatClicked">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="zAddItemToList" id="3ff6b099-008c-4d0d-a353-39ca18db6567">
        <source>zAddItemToList(listbox : ListBox; object : Object; text : String; level : Integer)protected;

vars

begin
	listbox.itemObject[listbox.addItem(text)] 	:= object;
	listbox.itemLevel[listbox.newIndex] 		:= level;
	listbox.itemExpanded[listbox.newIndex]		:= true;
end;
</source>
        <access>protected</access>
        <Parameter name="listbox">
            <type name="ListBox"/>
        </Parameter>
        <Parameter name="object">
            <type name="Object"/>
        </Parameter>
        <Parameter name="text">
            <type name="String"/>
        </Parameter>
        <Parameter name="level">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="zInitializeForm" id="af594aac-60e7-42de-9816-c157fd647f7e">
        <updating>true</updating>
        <source>zInitializeForm() updating, protected;

vars
	docitem : DocumentorPackage::DocumentInterface;	
	country	: Country;
	agent	: Agent;
	region	: Region;
begin
	//clear any existing list of documentation items
	app.myDocumentHub.clearDocumentationObjects();
	
	lstCandidates.clear;
	
	zAddItemToList(lstCandidates, null, "-- Agents --", 1);
	foreach docitem in app.myCompany.allAgents do
		//the end point of the listEntryDescription is different name in 
		//each implementing class, prefer to use docitem...
		zAddItemToList(lstCandidates, docitem.Object, docitem.listEntryDescription(), 2);
	endforeach;

	zAddItemToList(lstCandidates, null, "-- Regions --", 1);
	foreach country in app.myCompany.allCountries do
		foreach docitem in country.allRegions do
			//the end point of the listEntryDescription is different name in 
			//each implementing class, prefer to use docitem...
			zAddItemToList(lstCandidates, docitem.Object, docitem.listEntryDescription(), 2);
		endforeach;
	endforeach;
end;</source>
        <access>protected</access>
    </JadeMethod>
</GUIClass>
