<JadeWebServiceProviderClass name="ErewhonInvestmentsService" id="b7a45375-902b-45c2-a4a5-2ba5d638b94a">
    <superclass name="JadeWebServiceProvider"/>
    <transient>true</transient>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>This service is used to access client and agent information on the Erewhon system. 

A list of clients and or agent details can be obtained as well as details for an 
individual client or agent. 

Client and Agent details can also be updated using this service (2 variations).</text>
    <DbClassMap file="erewebservice"/>
    <JadeWebServicesMethod name="accessService" id="83b01841-f80a-4cf2-93e8-fc66f951ce16">
        <usesRPC>default</usesRPC>
        <source>accessService(action : String; _agent : Agent io; _client : Client io; names : StringArray output; photo : Binary output) : String webService;
/*	this is an example of a generalised Document/Literal web services method, which takes a parameter object and
	does a variety of actions depending on the action value.
	For requests, the agent or client will be updated.
	The return value is null if the action is successful, otherwise is an error message.
*/
vars
	agent			: Agent;
	client			: Client;
	sale			: Sale;
	replyMessage	: String;
	itemName		: String;
	result			: Integer;
begin
//	access activities
	if action = GET_AGENT then
		if _agent = null then
			replyMessage := "Entered agent is null";
		elseif _agent.name = "" then
			replyMessage := "Entered agent name is null";
		else
			itemName := _agent.name;
			_agent := app.myCompany.allAgents[itemName];
			if _agent = null then
				replyMessage := itemName &amp; " Agent does not exist";
			endif;
		endif;

	elseif action = GET_CLIENT then
		if _client = null then
			replyMessage := "Entered client is null";
		elseif _client.name = "" then
			replyMessage := "Entered client name is null";
		else
			itemName := _client.name;
			_client := app.myCompany.allClients[itemName];
			if _client = null then
				replyMessage := itemName &amp; " Client does not exist";
			endif;
		endif;

	elseif action = GET_AGENT_NAMES then
		create names transient;
		foreach agent in app.myCompany.allAgents do
			names.add(agent.name);
		endforeach;

	elseif action = GET_CLIENT_NAMES then
		create names transient;
		foreach client in app.myCompany.allClients do
			names.add(client.name);
		endforeach;

	elseif action = UPDATE_AGENT then
		if _agent = null then
			replyMessage := "Entered agent is null";
		elseif _agent.name = "" then
			replyMessage := "Entered agent name is null";
		else
			itemName := _agent.name;
			agent := app.myCompany.allAgents[itemName];
			if agent = null then
				replyMessage := itemName &amp; " Agent does not exist";
			else
				result := app.myTA.trxUpdateAgent(	agent,
													agent.edition,
													_agent.name,
													_agent.address1,
													_agent.address2,
													_agent.address3,
													_agent.phone,
													_agent.fax,
													_agent.email,
													_agent.webSite	);
										

				if result &lt;&gt; 0 then
					replyMessage := itemName &amp; " " &amp; global.getErrorString(app.getLastError);
				endif;
			endif;
		endif;

	elseif action = UPDATE_CLIENT then
		if _client = null then
			replyMessage := "Entered client is null";
		elseif _client.name = "" then
			replyMessage := "Entered client name is null";
		else
			itemName := _client.name;
			client := app.myCompany.allClients[itemName];
			if client = null then
				replyMessage := itemName &amp; " Client does not exist";
			else
				result := app.myTA.trxUpdateClient(	client,
													client.edition,
													_client.name,
													_client.address1,
													_client.address2,
													_client.address3,
													_client.phone,
													_client.fax,
													_client.email,
													_client.webSite	);
										

				if result &lt;&gt; 0 then
					replyMessage := itemName &amp; " " &amp; global.getErrorString(app.getLastError);
				endif;
			endif;
		endif;

	elseif action = GET_PHOTO then
		if _client = null then
			replyMessage := "Entered client is null";
		elseif _client.name = "" then
			replyMessage := "Entered client name is null";
		else
			client := app.myCompany.allClients[_client.name];
			if client = null then
				replyMessage := client.name &amp; " Client does not exist";
			else	
				foreach sale in client.allRetailSales do
					if sale.mySaleItem.photo &lt;&gt; null then
						photo := sale.mySaleItem.photo;
						break;
					endif;
				endforeach;
			endif;
		endif;

//	administration activities		
	elseif action = CREATE_AGENT then
		if _agent = null then
			replyMessage := "Entered agent is null";
		elseif _agent.name = "" then
			replyMessage := "Entered agent name is null";
		else
			itemName := _agent.name;
			agent := app.myCompany.allAgents[itemName];

			if agent &lt;&gt; null then
				return agent.name &amp; " Agent already exists";
			endif;

			result := app.myTA.trxCreateAgent(_agent.name,
											  _agent.address1,
											  _agent.address2,
											  _agent.address3,
											  _agent.phone,
											  _agent.fax,
											  _agent.email,
											  _agent.webSite,
											  agent	);

			if result &lt;&gt; 0 then
				replyMessage := _agent.name &amp; " " &amp; global.getErrorString(app.getLastError);
			endif;
		endif;
		return replyMessage;

	elseif action = CREATE_CLIENT then
		if _client = null then
			replyMessage := "Entered client is null";
		elseif _client.name = "" then
			replyMessage := "Entered client name is null";
		else
			itemName := _client.name;
			client := app.myCompany.allClients[itemName];

			if client &lt;&gt; null then
				replyMessage := _client.name &amp; " Client already exists";
			else
				result := app.myTA.trxCreateClient(	_client.name,
													_client.address1,
													_client.address2,
													_client.address3,
													_client.phone,
													_client.fax,
													_client.email,
													_client.webSite,
													client	);

				if result &lt;&gt; 0 then
					replyMessage := _client.name&amp; " " &amp; global.getErrorString(app.getLastError);
				endif;
			endif;
		endif;
		return replyMessage;

	elseif action = DELETE_AGENT then
		if _agent = null then
			replyMessage := "Entered agent is null";
		elseif _agent.name = "" then
			replyMessage := "Entered agent name is null";
		else
			itemName := _agent.name;
			agent := app.myCompany.allAgents[itemName];

			if agent = null then
				replyMessage := itemName &amp; " Agent does not exist";
			else
				result := app.myTA.trxDeleteAgent(agent);

				if result &lt;&gt; 0 then
					replyMessage := itemName &amp; " " &amp; global.getErrorString(app.getLastError);
				endif;										
			endif;
		endif;
		return replyMessage;
	
	elseif action = DELETE_CLIENT then
		if _client = null then
			replyMessage := "Entered client is null";
		elseif _client.name = "" then
			replyMessage := "Entered client name is null";
		else
			itemName := _client.name;
			client := app.myCompany.allClients[itemName];

			if client = null then
				replyMessage := itemName &amp; " Client does not exist";
			else

				result := app.myTA.trxDeleteClient(client);

				if result &lt;&gt; 0 then
					replyMessage := itemName &amp; " " &amp; global.getErrorString(app.getLastError);
				endif;
			endif;
		endif;
		return replyMessage;
		
	else
		replyMessage := "Unknown action: " &amp; action;
	endif;
	return replyMessage;
end;
</source>
        <Parameter name="action">
            <type name="String"/>
        </Parameter>
        <Parameter name="_agent">
            <usage>io</usage>
            <type name="Agent"/>
        </Parameter>
        <Parameter name="_client">
            <usage>io</usage>
            <type name="Client"/>
        </Parameter>
        <Parameter name="names">
            <usage>output</usage>
            <type name="StringArray"/>
        </Parameter>
        <Parameter name="photo">
            <usage>output</usage>
            <type name="Binary"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="getAgent" id="892ae1b0-1bed-4adf-a249-0a1a69e9ce34">
        <usesRPC>default</usesRPC>
        <updating>true</updating>
        <source>getAgent(agentName : String) : Agent webService, updating;

vars
	agent : Agent;
begin
	agent := app.myCompany.allAgents[agentName];
	
	if agent = null then
		setError(24, agentName, "Agent does not exist");
	endif;
	
	return agent;
end;
</source>
        <text>Given an agent name, this method will return an agent object. If an agent with the supplied name does not exist, error 24 will be returned.</text>
        <Parameter name="agentName">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Agent"/>
        </ReturnType>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="getAgentNames" id="3538a728-eaf1-404e-8ff6-bbceee09f986">
        <usesRPC>default</usesRPC>
        <source>getAgentNames() : StringArray webService;

vars
	names	: StringArray;
	agent	: Agent;
begin
	create names transient;
	foreach agent in app.myCompany.allAgents do
		names.add(agent.name);
	endforeach;
	return names;
end;
</source>
        <text>This method will return a string array of agent names.</text>
        <ReturnType>
            <type name="StringArray"/>
        </ReturnType>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="getClient" id="726e8cb0-f18b-415e-bb79-831e51998038">
        <usesRPC>default</usesRPC>
        <updating>true</updating>
        <source>getClient(clientName : String) : Client webService, updating;

vars
	client : Client;
begin
	client := app.myCompany.allClients[clientName];
	
	if client = null then
		setError(23, clientName, "Client does not exist");
	endif;
	
	return client;
end;
</source>
        <text>Given a client name, this method will return a client object. If a client with the supplied name does not exist, error 23 will be returned.</text>
        <Parameter name="clientName">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Client"/>
        </ReturnType>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="getClientNames" id="524d56fb-4f22-429c-985d-6d11fa918d08">
        <usesRPC>default</usesRPC>
        <source>getClientNames() : StringArray webService;

vars
	names	: StringArray;
	client	: Client;
begin
	create names transient;
	foreach client in app.myCompany.allClients do
		names.add(client.name);
	endforeach;

	return names;
end;
</source>
        <text>This method will return a string array of client names.</text>
        <ReturnType>
            <type name="StringArray"/>
        </ReturnType>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="getPhoto" id="fc7343c4-6b7e-4a9f-875d-b884011d7fef">
        <usesRPC>default</usesRPC>
        <updating>true</updating>
        <source>getPhoto(clientName : String):Binary updating, webService;

vars
	client : Client;
	sale : Sale;
begin
	client := getClient(clientName);
	if client = null then
		return null;
	endif;	

	foreach sale in client.allRetailSales do
		if sale.mySaleItem.photo &lt;&gt; null then
			return sale.mySaleItem.photo;
			break;
		endif;
	endforeach;
	return null;
end;</source>
        <Parameter name="clientName">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Binary"/>
        </ReturnType>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="updateAgent" id="8227c934-354f-4f2a-a440-3a0f3f1492e1">
        <usesRPC>default</usesRPC>
        <updating>true</updating>
        <source>updateAgent(_name, _address1, _address2, _address3, _phone, _fax, _email, _webSite : String) updating, webService;

vars
	agent 	: Agent;
	result	: Integer;
begin
	//perform value to object mapping
	agent := app.myCompany.allAgents[_name];
	
	if agent = null then
		setError(24, _name, "Agent does not exist");
		return;
	endif;
	
	result := app.myTA.trxUpdateAgent(	agent,
										agent.edition,
										_name,
										_address1,
										_address2,
										_address3,
										_phone,
										_fax,
										_email,
										_webSite	);
										
	if result &lt;&gt; 0 then
		setError(result, _name, global.getErrorString(app.getLastError));
	endif;										
end;
</source>
        <text>This method takes several string parameters and updates the persistent copy with details from the parameters. If the persistent object does not exist, error 24 is returned.</text>
        <Parameter name="_name">
            <type name="String"/>
        </Parameter>
        <Parameter name="_address1">
            <type name="String"/>
        </Parameter>
        <Parameter name="_address2">
            <type name="String"/>
        </Parameter>
        <Parameter name="_address3">
            <type name="String"/>
        </Parameter>
        <Parameter name="_phone">
            <type name="String"/>
        </Parameter>
        <Parameter name="_fax">
            <type name="String"/>
        </Parameter>
        <Parameter name="_email">
            <type name="String"/>
        </Parameter>
        <Parameter name="_webSite">
            <type name="String"/>
        </Parameter>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="updateAgentWithProxy" id="05cf7b86-4bff-485f-b71a-40e1eaecc639">
        <usesRPC>default</usesRPC>
        <updating>true</updating>
        <source>updateAgentWithProxy(proxyAgent : Agent) updating, webService;

vars
	agent 	: Agent;
	result	: Integer;
begin
	agent := app.myCompany.allAgents[proxyAgent.name];
	
	if agent = null then
		setError(24, proxyAgent.name, "Agent does not exist");
		return;
	endif;
	
	result := app.myTA.trxUpdateAgent(	agent,
										agent.edition,
										proxyAgent.name,
										proxyAgent.address1,
										proxyAgent.address2,
										proxyAgent.address3,
										proxyAgent.phone,
										proxyAgent.fax,
										proxyAgent.email,
										proxyAgent.webSite	);
										

	if result &lt;&gt; 0 then
		setError(result, proxyAgent.name, global.getErrorString(app.getLastError));
	endif;										
end;
</source>
        <Parameter name="proxyAgent">
            <type name="Agent"/>
        </Parameter>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="updateClient" id="53171a44-602b-4aad-a030-64066c8c856f">
        <usesRPC>default</usesRPC>
        <updating>true</updating>
        <source>updateClient(_name, _address1, _address2, _address3, _phone, _fax, _email, _webSite : String) updating, webService;

vars
	client 	: Client;
	result	: Integer;
begin
	client := app.myCompany.allClients[_name];
	
	if client = null then
		setError(23, _name, "Client does not exist");
		return;
	endif;
	
	result := app.myTA.trxUpdateClient(	client,
										client.edition,
										_name,
										_address1,
										_address2,
										_address3,
										_phone,
										_fax,
										_email,
										_webSite	);
										
	if result &lt;&gt; 0 then
		setError(result, _name, global.getErrorString(app.getLastError));
	endif;										
end;
</source>
        <text>This method takes several string parameters and updates the persistent copy with details from the parameters. If the persistent object does not exist, error 23 is returned.</text>
        <Parameter name="_name">
            <type name="String"/>
        </Parameter>
        <Parameter name="_address1">
            <type name="String"/>
        </Parameter>
        <Parameter name="_address2">
            <type name="String"/>
        </Parameter>
        <Parameter name="_address3">
            <type name="String"/>
        </Parameter>
        <Parameter name="_phone">
            <type name="String"/>
        </Parameter>
        <Parameter name="_fax">
            <type name="String"/>
        </Parameter>
        <Parameter name="_email">
            <type name="String"/>
        </Parameter>
        <Parameter name="_webSite">
            <type name="String"/>
        </Parameter>
    </JadeWebServicesMethod>
    <JadeWebServicesMethod name="updateClientWithProxy" id="5c5548ea-6069-4503-b1b9-f17149fa1211">
        <usesRPC>default</usesRPC>
        <updating>true</updating>
        <source>updateClientWithProxy(proxyClient : Client) updating, webService;

vars
	client 	: Client;
	result	: Integer;
begin
	client := app.myCompany.allClients[proxyClient.name];
	
	if client = null then
		setError(23, proxyClient.name, "Client does not exist");
		return;
	endif;
	
	result := app.myTA.trxUpdateClient(	client,
										client.edition,
										proxyClient.name,
										proxyClient.address1,
										proxyClient.address2,
										proxyClient.address3,
										proxyClient.phone,
										proxyClient.fax,
										proxyClient.email,
										proxyClient.webSite	);
										
	if result &lt;&gt; 0 then
		setError(result, proxyClient.name, global.getErrorString(app.getLastError));
	endif;										
end;
</source>
        <text>This method takes a client proxy object as parameter and updates the persistent copy of the client object with details of the proxy. If the persistent object does not exist, error 23 is returned.</text>
        <Parameter name="proxyClient">
            <type name="Client"/>
        </Parameter>
    </JadeWebServicesMethod>
</JadeWebServiceProviderClass>
