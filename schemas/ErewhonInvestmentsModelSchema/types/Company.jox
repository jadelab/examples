<Class name="Company" id="25dcc09f-c8c4-421d-afb3-54c393c487a7">
    <instanceVolatility>stable</instanceVolatility>
    <superclass name="AddressableEntity"/>
    <persistentAllowed>true</persistentAllowed>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <DbClassMap file="eredef"/>
    <ExplicitInverseRef name="allAgents" id="7e0863e9-4dd6-4293-a777-d7cdad2bf6dc">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="AgentByNameDict"/>
        <access>readonly</access>
        <Inverse name="Agent::myCompany"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="allClients" id="73c2cc54-f721-4454-bdfc-84623e1044e8">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="ClientByNameDict"/>
        <access>readonly</access>
        <Inverse name="Client::myCompany"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="allCountries" id="6ddfb252-f7fb-4cd4-abac-2e7f5844f82d">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="CountryByNameDict"/>
        <access>readonly</access>
        <Inverse name="Country::myCompany"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="allSaleItemCategories" id="562f6958-b0c0-45e9-99a5-0b1fd869c48b">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="SaleItemCategoryByNameDict"/>
        <access>readonly</access>
        <Inverse name="SaleItemCategory::myCompany"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="allSaleItems" id="c9dcde7f-efbb-446d-905f-002c98e595aa">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="SaleItemByCodeDict"/>
        <access>readonly</access>
        <Inverse name="SaleItem::myCompany"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="allSalesByItem" id="147d0bec-7b56-484e-928b-be9019b85e53">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="SaleByItemDict"/>
        <access>readonly</access>
        <Inverse name="Sale::myCompany"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="allTenderSaleItems" id="4e2d26ab-841c-4ee1-8fb3-f8de025dd89d">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="TenderItemBySaleDateCodeDict"/>
        <access>readonly</access>
        <Inverse name="SaleItem::myCompany"/>
    </ExplicitInverseRef>
    <JadeMethod name="closeTendersAtDate" id="ed9d4319-7644-4d5b-a42b-902fc3019a71">
        <executionLocation>server</executionLocation>
        <source>closeTendersAtDate(date : Date; numClosed : Integer output) serverExecution;

vars
	tenderSaleItem : TenderSaleItem;

begin
	// This operation assumes that it will always be called from within a transaction.
	// As such, it *does not* unlock any transaction duration locks that it acquires,
	// relying on the fact that all transaction duration locks are released at the
	// commit or abort transaction (manual unlocks of transaction duration locks
	// *during* a transaction are ignored anyway).
	// The following check asserts this assumption. It is not a necessary check and in
	// a production system may be removed. We've included it for illustrative purposes.
	if not process.isInTransactionState then
		app.raiseModelException(UpdOperationOutsideTranState);
	endif;

	numClosed := 0;

	foreach tenderSaleItem in allTenderSaleItems reversed do
		// Obtain an exclusive lock on the sale item to ensure we have its latest
		// edition before checking its mySale and closureDate, and to prevent it
		// from being updated by another process while this operation is running.
		exclusiveLock(tenderSaleItem);

		if tenderSaleItem.mySale &lt;&gt; null or tenderSaleItem.closureDate &gt; date then
			return;
		endif;

		if tenderSaleItem.acceptHighestTender &lt;&gt; null then
			numClosed := numClosed + 1;
		endif;
	endforeach;
end;</source>
        <Parameter name="date">
            <type name="Date"/>
        </Parameter>
        <Parameter name="numClosed">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="create" id="a641cb7a-4497-4cef-9882-9139c181a07d">
        <updating>true</updating>
        <source>create() updating;

begin
	// Only perform construction for persistent companies
	if not isTransient then
		// Enforce a singleton persistent Company. If we're creating a persistent
		// Company and one already exists, then raise an exception.
		if Company.firstInstance &lt;&gt; null then
			app.raiseModelException(CompanySingletonAlreadyExists);
		endif;
		// Initialize our singleton Company
		name		:= $CompanyName;
		address1	:= $CompanyAddress1;
		address2	:= $CompanyAddress2;
		address3	:= $CompanyAddress3;
		email		:= $CompanyEMail;
		fax			:= $CompanyFax;
		phone		:= $CompanyPhone;
		webSite		:= $CompanyWebSite;
	endif;
end;</source>
    </JadeMethod>
    <JadeMethod name="createAgent" id="11a2a9a9-377d-4a96-9ccb-7219c8b3be07">
        <source>createAgent(
		agentName : String;
		address1  : String;
		address2  : String;
		address3  : String;
		phone     : String;
		fax       : String;
		email     : String;
		webSite   : String) : Agent;
// --------------------------------------------------------------------------------
// Method:		createAgent
//
// Purpose:		Creates a new Agent for the receiver Company
//
// Returns:     The newly created Agent
// --------------------------------------------------------------------------------
vars
	agent : Agent;

begin
	create agent persistent;
	agent.updateForCreate(agentName, address1, address2,
			address3, phone, fax, email, webSite, self);
	return agent;
end;</source>
        <Parameter name="agentName">
            <type name="String"/>
        </Parameter>
        <Parameter name="address1">
            <type name="String"/>
        </Parameter>
        <Parameter name="address2">
            <type name="String"/>
        </Parameter>
        <Parameter name="address3">
            <type name="String"/>
        </Parameter>
        <Parameter name="phone">
            <type name="String"/>
        </Parameter>
        <Parameter name="fax">
            <type name="String"/>
        </Parameter>
        <Parameter name="email">
            <type name="String"/>
        </Parameter>
        <Parameter name="webSite">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Agent"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="createCategory" id="290a97c0-7e77-40a3-a24c-26badb4ed0cd">
        <source>createCategory(categoryName : String; categoryDescription : String) : SaleItemCategory;
// --------------------------------------------------------------------------------
// Method:		createCategory
//
// Purpose:		Creates a new SaleItemCategory for the receiver Company
//
// Returns:     The newly created SaleItemCategory
// --------------------------------------------------------------------------------
vars
	saleItemCategory : SaleItemCategory;

begin
	create saleItemCategory persistent;
	saleItemCategory.updateForCreate(categoryName, categoryDescription, self);
	return saleItemCategory;
end;</source>
        <Parameter name="categoryName">
            <type name="String"/>
        </Parameter>
        <Parameter name="categoryDescription">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="SaleItemCategory"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="createClient" id="6590b30c-d19a-4e2e-80dd-c759b0172bcf">
        <source>createClient(
		clientName : String;
		address1   : String;
		address2   : String;
		address3   : String;
		phone      : String;
		fax        : String;
		email      : String;
		webSite    : String) : Client;
// --------------------------------------------------------------------------------
// Method:		createClient
//
// Purpose:		Creates a new Client for the receiver Company
//
// Returns:     The newly created Client
// --------------------------------------------------------------------------------
vars
	client : Client;

begin
	create client persistent;
	client.updateForCreate(clientName, address1, address2, address3, phone, fax, email, webSite, self);
	return client;
end;</source>
        <Parameter name="clientName">
            <type name="String"/>
        </Parameter>
        <Parameter name="address1">
            <type name="String"/>
        </Parameter>
        <Parameter name="address2">
            <type name="String"/>
        </Parameter>
        <Parameter name="address3">
            <type name="String"/>
        </Parameter>
        <Parameter name="phone">
            <type name="String"/>
        </Parameter>
        <Parameter name="fax">
            <type name="String"/>
        </Parameter>
        <Parameter name="email">
            <type name="String"/>
        </Parameter>
        <Parameter name="webSite">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Client"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="createCountry" id="f691ac00-7776-4512-8866-0d5549a68b8c">
        <source>createCountry(countryName : String) : Country;
// --------------------------------------------------------------------------------
// Method:		createCountry
//
// Purpose:		Creates a new Country for the receiver Company
//
// Returns:     The newly created Country
// --------------------------------------------------------------------------------
vars
	country : Country;

begin
	create country persistent;
	country.updateForCreate(countryName, self);
	return country;
end;</source>
        <Parameter name="countryName">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Country"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="createRetailSale" id="ff099f4e-4bfb-4355-83c2-e8b2c51855b2">
        <source>createRetailSale(retailSaleItem : RetailSaleItem;
                 client         : Client;
                 price          : Decimal;
                 timeStamp      : TimeStamp) : RetailSale;
// --------------------------------------------------------------------------------
// Method:		createRetailSale
//
// Purpose:		Creates a new RetailSale for the supplied RetailSaleItem and Client
//
// Returns:     The newly created RetailSale
// --------------------------------------------------------------------------------
vars
	retailSale : RetailSale;

begin
	create retailSale persistent;
	retailSale.updateForCreate(retailSaleItem, client, price, timeStamp);
	return retailSale;
end;</source>
        <Parameter name="retailSaleItem">
            <type name="RetailSaleItem"/>
        </Parameter>
        <Parameter name="client">
            <type name="Client"/>
        </Parameter>
        <Parameter name="price">
            <type name="Decimal"/>
        </Parameter>
        <Parameter name="timeStamp">
            <type name="TimeStamp"/>
        </Parameter>
        <ReturnType>
            <type name="RetailSale"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="createRetailSaleItem" id="738754fc-e9cb-4519-8930-b2a53d706ea8">
        <source>createRetailSaleItem(
		codePrefix       : String;
		shortDescription : String;
		fullDescription  : String;
		forSaleDate      : Date;
		photo            : Binary;
		agent            : Agent;
		region           : Region;
		saleItemCategory : SaleItemCategory;
		price            : Decimal) : RetailSaleItem;
// --------------------------------------------------------------------------------
// Method:		createRetailSaleItem
//
// Purpose:		Creates a new RetailSaleItem for the receiver Company, with the
//              new item belonging to the supplied Region and SaleItemCategory
//
// Returns:     The newly created RetailSaleItem
// --------------------------------------------------------------------------------
vars
	retailSaleItem : RetailSaleItem;
	codeNumber     : Integer;

begin
	create retailSaleItem persistent;
	codeNumber := getNextCodeNumberForPrefix(codePrefix);

	retailSaleItem.updateForCreate(codePrefix, codeNumber,
			shortDescription, fullDescription, forSaleDate, photo,
			agent, region, saleItemCategory, price, self);
			
	return retailSaleItem;
end;</source>
        <Parameter name="codePrefix">
            <type name="String"/>
        </Parameter>
        <Parameter name="shortDescription">
            <type name="String"/>
        </Parameter>
        <Parameter name="fullDescription">
            <type name="String"/>
        </Parameter>
        <Parameter name="forSaleDate">
            <type name="Date"/>
        </Parameter>
        <Parameter name="photo">
            <type name="Binary"/>
        </Parameter>
        <Parameter name="agent">
            <type name="Agent"/>
        </Parameter>
        <Parameter name="region">
            <type name="Region"/>
        </Parameter>
        <Parameter name="saleItemCategory">
            <type name="SaleItemCategory"/>
        </Parameter>
        <Parameter name="price">
            <type name="Decimal"/>
        </Parameter>
        <ReturnType>
            <type name="RetailSaleItem"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="createTenderSale" id="43797c17-2173-460a-93a5-be36a6c2b02f">
        <source>createTenderSale(tenderSaleItem : TenderSaleItem; tender : Tender) : TenderSale;
// --------------------------------------------------------------------------------
// Method:		createTenderSale
//
// Purpose:		Creates a new TenderSale for the supplied TenderSaleItem and Client
//
// Returns:     The newly created TenderSale
// --------------------------------------------------------------------------------
vars
	tenderSale : TenderSale;

begin
	create tenderSale persistent;
	tenderSale.updateForCreate(tenderSaleItem, tender);
	return tenderSale;
end;</source>
        <Parameter name="tenderSaleItem">
            <type name="TenderSaleItem"/>
        </Parameter>
        <Parameter name="tender">
            <type name="Tender"/>
        </Parameter>
        <ReturnType>
            <type name="TenderSale"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="createTenderSaleItem" id="fc7374c8-2dc4-47e1-a878-f5ff16fd01ec">
        <source>createTenderSaleItem(
		codePrefix       : String;
		shortDescription : String;
		fullDescription  : String;
		forSaleDate      : Date;
		photo            : Binary;
		agent            : Agent;
		region           : Region;
		saleItemCategory : SaleItemCategory;
		closureDate      : Date;
		minimumPrice     : Decimal) : TenderSaleItem;
// --------------------------------------------------------------------------------
// Method:		createTenderSaleItem
//
// Purpose:		Creates a new TenderSaleItem for the receiver Company, with the
//              new item belonging to the supplied Region and SaleItemCategory
//
// Returns:     The newly created TenderSaleItem
// --------------------------------------------------------------------------------
vars
	tenderSaleItem	: TenderSaleItem;
	codeNumber		: Integer;

begin
	create tenderSaleItem persistent;
	codeNumber := getNextCodeNumberForPrefix(codePrefix);

	tenderSaleItem.updateForCreate(
			codePrefix, codeNumber, shortDescription, fullDescription,
			forSaleDate, photo, agent, region, saleItemCategory,
			closureDate, minimumPrice, self);
	return tenderSaleItem;
end;</source>
        <Parameter name="codePrefix">
            <type name="String"/>
        </Parameter>
        <Parameter name="shortDescription">
            <type name="String"/>
        </Parameter>
        <Parameter name="fullDescription">
            <type name="String"/>
        </Parameter>
        <Parameter name="forSaleDate">
            <type name="Date"/>
        </Parameter>
        <Parameter name="photo">
            <type name="Binary"/>
        </Parameter>
        <Parameter name="agent">
            <type name="Agent"/>
        </Parameter>
        <Parameter name="region">
            <type name="Region"/>
        </Parameter>
        <Parameter name="saleItemCategory">
            <type name="SaleItemCategory"/>
        </Parameter>
        <Parameter name="closureDate">
            <type name="Date"/>
        </Parameter>
        <Parameter name="minimumPrice">
            <type name="Decimal"/>
        </Parameter>
        <ReturnType>
            <type name="TenderSaleItem"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getAllRegions" id="12586692-43a4-44e7-8b99-3f840f58316c">
        <source>getAllRegions(regionSet : RegionSet input);
// --------------------------------------------------------------------------------
// Method:		getAllRegions
//
// Purpose:		Return a snapshot of all regions for the receiver Company
//
// Parameters:	The region set in which the company's regions will be returned     
// --------------------------------------------------------------------------------
vars
	country : Country;

begin
	// For each country in the company, copy the country's regions into the set
	foreach country in allCountries do
		country.allRegions.copy(regionSet);
	endforeach;
end;</source>
        <Parameter name="regionSet">
            <usage>input</usage>
            <type name="RegionSet"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="getAllSales" id="f5227c39-0379-4ba2-9c7b-b6ab38e9569a">
        <source>getAllSales(saleSet : SaleSet input);
// --------------------------------------------------------------------------------
// Method:		getAllSales
//
// Purpose:		Returns a snapshot of all sales for the company
//
// Parameters:	The sale set in which the sales are to be returned
// --------------------------------------------------------------------------------
begin
	// If the company has some sales, copy them into the supplied set
	if not allSalesByItem.isEmpty then
		allSalesByItem.copy(saleSet);
	endif;
end;</source>
        <Parameter name="saleSet">
            <usage>input</usage>
            <type name="SaleSet"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="getNextCodeNumberForPrefix" id="7e5939a4-f121-453d-a479-97ec1d1c007a">
        <source>getNextCodeNumberForPrefix(prefix : String) : Integer;
// --------------------------------------------------------------------------------
// Method:		getNextCodeNumberForPrefix
//
// Purpose:		Given a prefix string, this method will return the next code number
//          	for the prefix. This method should always be called from within a
//          	transaction as the number returned is valid only for the duration
//          	of the allSaleItems shared lock.
//
// Parameters:	The prefix for which the next number is to be returned.
//
// Returns:		The newly allocated code number for the prefix.
// --------------------------------------------------------------------------------
vars
	saleItem : SaleItem;

begin
	// This should always be called from within a transaction as the number
	// returned is valid only for the duration of the allSaleItems shared lock
	if not process.isInTransactionState then
		app.raiseModelException(CodeAllocationOutsideTranState);
	endif;

	// Share lock the allSaleItems collection on the company to prevent any sale
	// items from being added (read access is still permitted though)
	sharedLock(allSaleItems);

	// Find the highest sale item key for the given prefix
	saleItem := allSaleItems.getAtKeyLeq(prefix, Max_Integer);

	// If we didn't find an item then there are no existing items with the supplied
	// prefix. We return 1.
	if saleItem = null then
		return 1;
	endif;

	// Ensure that we have the latest edition of the sale item in order to compare
	// its prefix and codeNumber. We also don't want any other process updating it
	// until we've finished.
	sharedLock(saleItem);

	// If the item doesn't have the supplied prefix it means there are no existing
	// items with the supplied prefix. We return 1.
	if saleItem.codePrefix &lt;&gt; prefix then
		return 1;
	endif;

	// If the number of the item is Max_Integer then we cannot allocated any more
	// numbers for the supplied prefix	
	if saleItem.codeNumber = Max_Integer then
		app.raiseModelException(TooManyCodeNumbersForPrefix);
	endif;

	// Otherwise return the next code number
	return saleItem.codeNumber + 1;
end;</source>
        <Parameter name="prefix">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getNumberOfLocations" id="1a809990-2c52-4f32-98a8-faba60bf8ffb">
        <source>getNumberOfLocations() : Integer;
// --------------------------------------------------------------------------------
// Method:		getNumberOfLocations
//
// Purpose:		Return the number of locations for the receiver company
// --------------------------------------------------------------------------------
vars
	country : Country;
	total   : Integer;

begin
	// allCountries is automatically share locked by the foreach, so it won't be
	// updated while we're iterating
	foreach country in allCountries do
		total := total + 1 + country.allRegions.size;
	endforeach;
	return total;
end;</source>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getRegion" id="0da62ca1-428b-4975-a425-9d2149777535">
        <source>getRegion(countryName, regionName : String) : Region;
// --------------------------------------------------------------------------------
// Method:		getRegion
//
// Purpose:		Returns the region for the specified country and region name; or
//              null if the country or region don't exist
// --------------------------------------------------------------------------------
vars
	country : Country;

begin
	country := allCountries[countryName];
	if country &lt;&gt; null then
		return country.allRegions[regionName];
	endif;
	return null;
end;</source>
        <Parameter name="countryName">
            <type name="String"/>
        </Parameter>
        <Parameter name="regionName">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Region"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getRetailSaleItem" id="0a0853c8-0adb-406e-ae16-48a144ec6800">
        <source>getRetailSaleItem(itemCode : String) : RetailSaleItem;
// --------------------------------------------------------------------------------
// Method:		getRetailSaleItem
//
// Purpose:		Return the retail sale item for the supplied code.
//              If no item is found or the item is not a retail item, return null.
// --------------------------------------------------------------------------------
vars
	codePrefix : String;
	codeNumber : Integer;
	saleItem   : SaleItem;

begin
	// Separate the code string into its prefix and number
	itemCode.getCodePrefixAndNumber(codePrefix, codeNumber);

	saleItem := allSaleItems[codePrefix, codeNumber];

	if saleItem = null or not saleItem.isKindOf(RetailSaleItem) then
		return null;
	endif;

	return saleItem.RetailSaleItem;
end;</source>
        <Parameter name="itemCode">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="RetailSaleItem"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getTenderSaleItem" id="3ecf8f7c-fedc-4239-b763-9e6780d9e026">
        <source>getTenderSaleItem(itemCode : String) : TenderSaleItem;
// --------------------------------------------------------------------------------
// Method:		getTenderSaleItem
//
// Purpose:		Return the tender sale item for the supplied code.
//              If no item is found or the item is not a tender item, return null.
// --------------------------------------------------------------------------------
vars
	codePrefix : String;
	codeNumber : Integer;
	saleItem   : SaleItem;

begin
	// Separate the code string into its prefix and number
	itemCode.getCodePrefixAndNumber(codePrefix, codeNumber);

	saleItem := allSaleItems[codePrefix, codeNumber];

	if saleItem = null or not saleItem.isKindOf(TenderSaleItem) then
		return null;
	endif;

	return saleItem.TenderSaleItem;
end;</source>
        <Parameter name="itemCode">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="TenderSaleItem"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="update" id="45e2c09a-78ac-4c46-b392-017b01b34b83">
        <updating>true</updating>
        <source>update(parName     : String;
       parAddress1 : String;
       parAddress2 : String;
       parAddress3 : String;
       parPhone    : String;
       parFax      : String;
       parEmail    : String;
       parWebSite  : String) updating;
// --------------------------------------------------------------------------------
// Method:		update
//
// Purpose:		Update just the attributes (ie: no references) of a Company.
//              This method should be used to update an existing Company.
// --------------------------------------------------------------------------------
begin
	// This operation assumes that it will always be called from within a transaction.
	// As such, it *does not* unlock any transaction duration locks that it acquires,
	// relying on the fact that all transaction duration locks are released at the
	// commit or abort transaction (manual unlocks of transaction duration locks
	// *during* a transaction are ignored anyway).
	// The following check asserts this assumption. It is not a necessary check and in
	// a production system may be removed. We've included it for illustrative purposes.
	if not process.isInTransactionState then
		app.raiseModelException(UpdOperationOutsideTranState);
	endif;

	// We're comparing parameter values with our property values so we must ensure we have
	// the latest edition of ourself before we compare anything. Obtain an exclusive lock
	// because we're going to be updating ourself anyway.
	// It is more efficient if our caller has locked us. It they haven't and "self" was not
	// in cache when they called us, we will go to the server twice: once to get our object
	// to dispatch the method, and then again here to place the exclusive lock. If our
	// caller locks us before calling this method, we will go to the server only once for
	// the lock (as a lock brings the latest edition of an object into cache).
	// We do an exclusive lock here as each operation takes responsibility for its own
	// integrity. The operation must be safe no matter what the caller has or hasn't locked.
	// If we are *already* locked, the exclusiveLock we do here *does not* go to the server.
	exclusiveLock(self);
	
	self.zUpdateAddressableEntityCommon(parName, parAddress1, parAddress2,
			parAddress3, parPhone, parFax, parEmail, parWebSite, self);
end;</source>
        <Parameter name="parName">
            <type name="String"/>
        </Parameter>
        <Parameter name="parAddress1">
            <type name="String"/>
        </Parameter>
        <Parameter name="parAddress2">
            <type name="String"/>
        </Parameter>
        <Parameter name="parAddress3">
            <type name="String"/>
        </Parameter>
        <Parameter name="parPhone">
            <type name="String"/>
        </Parameter>
        <Parameter name="parFax">
            <type name="String"/>
        </Parameter>
        <Parameter name="parEmail">
            <type name="String"/>
        </Parameter>
        <Parameter name="parWebSite">
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="writeSomething" id="b34fb216-e8af-4dc0-bad7-5f8ab136e9b4">
        <source>writeSomething();

vars

begin
	app.msgBox("Hello","Hello",MsgBox_OK_Only);
end;
</source>
    </JadeMethod>
    <JadeMethod name="zDuplicateCheck" id="19286f08-16e8-4805-b669-bbe033f969b2">
        <source>zDuplicateCheck(parCompany : Company; parTrimmedName: String) protected;
// --------------------------------------------------------------------------------
// Method:		zDuplicateCheck
//
// Purpose:		Check if name is already in use.
//				Not relevant to the Company class.
// --------------------------------------------------------------------------------
begin
	// Do nothing
end;</source>
        <access>protected</access>
        <Parameter name="parCompany">
            <type name="Company"/>
        </Parameter>
        <Parameter name="parTrimmedName">
            <type name="String"/>
        </Parameter>
    </JadeMethod>
</Class>
