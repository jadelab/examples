<Class name="Tender" id="53b3b582-468f-408b-ac02-0e439a7b6eb4">
    <superclass name="ModelEntity"/>
    <persistentAllowed>true</persistentAllowed>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <DbClassMap file="eretendr"/>
    <ExplicitInverseRef name="myClient" id="6019a095-fde3-4f7c-97e2-718c87823df3">
        <kind>child</kind>
        <updateMode>manual</updateMode>
        <embedded>true</embedded>
        <type name="Client"/>
        <access>readonly</access>
        <Inverse name="Client::allTenders"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="myTenderSale" id="74734fd3-69ab-4699-8fd7-5c727c1624f3">
        <updateMode>automatic</updateMode>
        <embedded>true</embedded>
        <type name="TenderSale"/>
        <access>readonly</access>
        <Inverse name="TenderSale::myTender"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="myTenderSaleItem" id="246be5be-886a-4b52-910e-9a38a82bfce6">
        <kind>child</kind>
        <updateMode>manual</updateMode>
        <embedded>true</embedded>
        <type name="TenderSaleItem"/>
        <access>readonly</access>
        <Inverse name="TenderSaleItem::allTendersByOfferTime"/>
        <Inverse name="TenderSaleItem::allTendersByTimeOffer"/>
    </ExplicitInverseRef>
    <PrimAttribute name="offer" id="494611cc-33c9-40fd-904d-f6edfdf39633">
        <precision>12</precision>
        <scaleFactor>2</scaleFactor>
        <embedded>true</embedded>
        <type name="Decimal"/>
        <access>readonly</access>
    </PrimAttribute>
    <PrimAttribute name="timeStamp" id="ff5da523-3af2-4165-b892-2c3452a4e6e1">
        <embedded>true</embedded>
        <type name="TimeStamp"/>
        <access>readonly</access>
    </PrimAttribute>
    <JadeMethod name="getDate" id="b9541836-acbc-47d8-a4a8-c0eb57e73564">
        <source>getDate() : Date;
// --------------------------------------------------------------------------------
// Method:		getDate
//
// Returns:     The offer date of the tender
// --------------------------------------------------------------------------------
begin
	return timeStamp.date;
end;</source>
        <ReturnType>
            <type name="Date"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getDebugString" id="a1619d48-3b4d-441b-a375-97a3642775f5">
        <source>getDebugString() : String;
// --------------------------------------------------------------------------------
// Method:		getDebugString
//
// Purpose:		Returns a debug string for a Tender
// --------------------------------------------------------------------------------
vars
	str : String;

begin
	str := inheritMethod &amp; ": $" &amp; offer.String &amp; ", " &amp; timeStamp.String &amp; ", ";

	if myClient = null then
		return str &amp; "null client";
	endif;

	return str &amp; myClient.name;
end;</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getTime" id="c6973e87-1a04-46b0-9c1b-59ed3eb1cdf4">
        <source>getTime() : Time;
// --------------------------------------------------------------------------------
// Method:		getDate
//
// Returns:     The offer time of the tender
// --------------------------------------------------------------------------------
begin
	return timeStamp.time;
end;</source>
        <ReturnType>
            <type name="Time"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="update" id="86d5ade9-ae1c-43c8-afc1-59dee07b3d1c">
        <updating>true</updating>
        <source>update(parOffer : Decimal; parTimeStamp : TimeStamp) updating;
// --------------------------------------------------------------------------------
// Method:		update
//
// Purpose:		Update just the attributes (ie: no references) of a Tender.
//              This method should be used to update an existing Tender.
//              Use updateForCreate to set properties on a newly created object.
// --------------------------------------------------------------------------------
vars
	saleItem : TenderSaleItem;
	
begin
	// This operation assumes that it will always be called from within a transaction.
	// As such, it *does not* unlock any transaction duration locks that it acquires,
	// relying on the fact that all transaction duration locks are released at the
	// commit or abort transaction (manual unlocks of transaction duration locks
	// *during* a transaction are ignored anyway).
	// The following check asserts this assumption. It is not a necessary check and in
	// a production system may be removed. We've included it for illustrative purposes.
	if not process.isInTransactionState then
		app.raiseModelException(UpdOperationOutsideTranState);
	endif;

	// We're comparing parameter values with our property values so we must ensure we have
	// the latest edition of ourself before we compare anything. Obtain an exclusive lock
	// because we're going to be updating ourself anyway.
	// It is more efficient if our caller has locked us. It they haven't and "self" was not
	// in cache when they called us, we will go to the server twice: once to get our object
	// to dispatch the method, and then again here to place the exclusive lock. If our
	// caller locks us before calling this method, we will go to the server only once for
	// the lock (as a lock brings the latest edition of an object into cache).
	// We do an exclusive lock here as each operation takes responsibility for its own
	// integrity. The operation must be safe no matter what the caller has or hasn't locked.
	// If we are *already* locked, the exclusiveLock we do here *does not* go to the server.
	exclusiveLock(self);

	if parTimeStamp = null or not parTimeStamp.isValid then
		app.raiseModelException(InvalidDateOrTime);
	endif;
	
	if parOffer &lt;= 0 then
		app.raiseModelException(InvalidOfferPrice);
	endif;
	
	saleItem := self.myTenderSaleItem;
	if saleItem = null then
		app.raiseModelException(InvalidSaleItem);
	endif;
	
	// We need the latest edition of the sale item to ensure that we'll see its
	// correct minimumPrice. We do not use an exclusive lock as we are not
	// going to update it.
	sharedLock(saleItem);
	
	if parOffer &lt; saleItem.minimumPrice then
		app.raiseModelException(OfferPriceLessThanMinPrice);
	endif;

	// Only update key properties if they have changed.
	// This avoids unnecessary automatic key maintenance of inverse dictionaries.
	if self.offer &lt;&gt; parOffer then
		self.offer := parOffer;
	endif;

	if self.timeStamp &lt;&gt; parTimeStamp then
		self.timeStamp := parTimeStamp;
	endif;
end;</source>
        <Parameter name="parOffer">
            <type name="Decimal"/>
        </Parameter>
        <Parameter name="parTimeStamp">
            <type name="TimeStamp"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="updateForCreate" id="6ff9da8c-d3ed-4891-bed0-1c60cd246bbb">
        <updating>true</updating>
        <source>updateForCreate(
		parOffer          : Decimal;
		parTimeStamp      : TimeStamp;
		parClient         : Client;
		parTenderSaleItem : TenderSaleItem) updating;
// --------------------------------------------------------------------------------
// Method:		updateForCreate
//
// Purpose:		Set properties when creating a Tender.
//              This method should be used only when creating a Tender.
//              It should not be used to update an existing object.
// --------------------------------------------------------------------------------
vars
	testTender : Tender;
	
begin
	if parClient = null then
		app.raiseModelException(InvalidClient);
	endif;

	if parTenderSaleItem = null then
		app.raiseModelException(InvalidSaleItem);
	endif;

	if parOffer &lt;= 0 then
		app.raiseModelException(InvalidOfferPrice);
	endif;
	
	if parTimeStamp = null or not parTimeStamp.isValid then
		app.raiseModelException(InvalidDateOrTime);
	endif;
	
	// We need the latest edition of the tender sale item to ensure that we'll see its
	// correct minimumPrice. We exclusively lock it because the tender sale item will
	// be updated anyway via inverse maintenance when we set myTenderSaleItem to it.
	exclusiveLock(parTenderSaleItem);

	if parOffer &lt; parTenderSaleItem.minimumPrice then
		app.raiseModelException(OfferPriceLessThanMinPrice);
	endif;

	testTender := parTenderSaleItem.allTendersByOfferTime[parOffer, parTimeStamp];
	if testTender &lt;&gt; null then
		app.raiseModelException(TenderAlreadyExists);
	endif;
	
	// Set key properties before references so that we don't update collection inverses
	// twice (if references are set before keys, inverse collections are updated once
	// when the reference is set and again when the key is set).
	self.offer            := parOffer;
	self.timeStamp        := parTimeStamp;
	self.myTenderSaleItem := parTenderSaleItem;

	// Set reference to parent
	self.myClient := parClient;
end;</source>
        <Parameter name="parOffer">
            <type name="Decimal"/>
        </Parameter>
        <Parameter name="parTimeStamp">
            <type name="TimeStamp"/>
        </Parameter>
        <Parameter name="parClient">
            <type name="Client"/>
        </Parameter>
        <Parameter name="parTenderSaleItem">
            <type name="TenderSaleItem"/>
        </Parameter>
    </JadeMethod>
</Class>
