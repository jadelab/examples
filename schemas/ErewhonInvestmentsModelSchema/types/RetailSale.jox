<Class name="RetailSale" id="151ae609-363e-44e0-9978-3ff4b0d7f3a5">
    <superclass name="Sale"/>
    <persistentAllowed>true</persistentAllowed>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <DbClassMap file="eresale"/>
    <PrimAttribute name="price" id="f769dbd4-d670-4109-a926-c754fba5a0e2">
        <precision>12</precision>
        <scaleFactor>2</scaleFactor>
        <embedded>true</embedded>
        <type name="Decimal"/>
        <access>readonly</access>
    </PrimAttribute>
    <PrimAttribute name="timeStamp" id="3c795fd8-d39f-4b68-b7d9-aea60018a3a7">
        <embedded>true</embedded>
        <type name="TimeStamp"/>
        <access>readonly</access>
    </PrimAttribute>
    <JadeMethod name="getAmount" id="ea7c61c6-8276-40f3-9b13-4c032a0f6e28">
        <source>getAmount() : Decimal;
// --------------------------------------------------------------------------------
// Method:		getAmount
//
// Returns:     The price of the retail sale
// --------------------------------------------------------------------------------
begin
	return price;
end;</source>
        <ReturnType>
            <type name="Decimal"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getDate" id="35c88c11-2e50-48c8-9201-697328b04f06">
        <source>getDate() : Date;
// --------------------------------------------------------------------------------
// Method:		getDate
//
// Returns:     The date of the retail sale
// --------------------------------------------------------------------------------
begin
	return timeStamp.date;
end;</source>
        <ReturnType>
            <type name="Date"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getDebugString" id="d20c7924-b6d3-468b-a436-fff170a8d614">
        <source>getDebugString() : String;
// --------------------------------------------------------------------------------
// Method:		getDebugString
//
// Purpose:		Returns a debug string for a RetailSale
// --------------------------------------------------------------------------------
vars
	str : String;

begin
	str := inheritMethod &amp; ": $" &amp; price.String &amp; ", " &amp; timeStamp.String &amp; ", ";

	if mySaleItem = null then
		str := str &amp; "null item, ";
	else
		str := str &amp; mySaleItem.getCode &amp; " (" &amp; mySaleItem.shortDescription &amp; "), ";
	endif;

	if myClient = null then
		str := str &amp; "null client";
	else
		str := str &amp; myClient.name;
	endif;

	return str;
end;</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getDiscountAmount" id="803a2e4f-f4ce-4ba3-b87a-d1cb27444503">
        <source>getDiscountAmount() : Decimal;
// --------------------------------------------------------------------------------
// Method:		getDiscountAmount
//
// Returns:     The discount on the retail sale (if the item was sold for less
//              than its published price)
// --------------------------------------------------------------------------------
vars
	retailPrice : Decimal[12, 2];

begin
	if mySaleItem &lt;&gt; null then
		// Ensure that we have the latest edition of our sale item
		sharedLock(mySaleItem);

		retailPrice := mySaleItem.RetailSaleItem.price;
		if price &lt; retailPrice then
			return retailPrice - price;
		endif;
	endif;

	return 0;

epilog
	// Release our locks in the epilog so that we'll release them even if we exit via an
	// exception. If we don't have an object locked, the unlock does nothing. If we're
	// in transaction state, transaction duration locks won't be released until the next
	// commit or abort transaction (even if we explicitly unlock them). That allows a
	// method to be well behaved and release its locks without having to be aware of the
	// transaction state of the process.
	if mySaleItem &lt;&gt; null then
		unlock(mySaleItem);
	endif;
end;</source>
        <ReturnType>
            <type name="Decimal"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getTime" id="3403ab6a-9750-4ed9-8aac-faba2aeaf8c1">
        <source>getTime() : Time;
// --------------------------------------------------------------------------------
// Method:		getTime
//
// Returns:     The time of the retail sale
// --------------------------------------------------------------------------------
begin
	return timeStamp.time;
end;</source>
        <ReturnType>
            <type name="Time"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="updateForCreate" id="e5da9dc4-5417-4e55-8b66-9712620fc81e">
        <updating>true</updating>
        <source>updateForCreate(
		parRetailSaleItem : RetailSaleItem;
		parClient         : Client;
		parPrice          : Decimal;
		parTimeStamp      : TimeStamp) updating;
// --------------------------------------------------------------------------------
// Method:		updateForCreate
//
// Purpose:		Set properties when creating a RetailSale.
//              This method should be used only when creating a RetailSale.
//              It should not be used to update an existing object.
// --------------------------------------------------------------------------------
begin
	if parRetailSaleItem = null then
		app.raiseModelException(InvalidSaleItem);
	endif;

	if parClient = null then
		app.raiseModelException(InvalidClient);
	endif;
	
	if not parPrice &gt; 0 then
		app.raiseModelException(InvalidPrice);
	endif;
	
	if parTimeStamp = null or not parTimeStamp.isValid then
		app.raiseModelException(InvalidDateOrTime);
	endif;

	// This operation assumes that it will always be called from within a transaction.
	// As such, it *does not* unlock any transaction duration locks that it acquires,
	// relying on the fact that all transaction duration locks are released at the
	// commit or abort transaction (manual unlocks of transaction duration locks
	// *during* a transaction are ignored anyway).
	// The following check asserts this assumption. It is not a necessary check and in
	// a production system may be removed. We've included it for illustrative purposes.
	if not process.isInTransactionState then
		app.raiseModelException(UpdOperationOutsideTranState);
	endif;

	// We need the latest edition of the retail sale item to ensure that we'll see its
	// correct myCompany reference (see below). We exclusively lock it because the sale
	// item will be updated anyway via inverse maintenance when we set mySaleItem to it.
	exclusiveLock(parRetailSaleItem);
	
	if parRetailSaleItem.mySale &lt;&gt; null then
		app.raiseModelException(ItemAlreadySold);
	endif;
	
	if parRetailSaleItem.myCompany = null then
		app.raiseModelException(InvalidCompany);
	endif;

	// Set key properties before references so that we don't update collection inverses
	// twice (if references are set before keys, inverse collections are updated once
	// when the reference is set and again when the key is set).
	self.timeStamp  := parTimeStamp;
	self.mySaleItem := parRetailSaleItem;

	// Set the remaining properties
	self.price      := parPrice;

	// Set references to other parents
	self.myClient   := parClient;
	self.myCompany  := parRetailSaleItem.myCompany;

	// Calculate the agent's commission on the sale
	self.zCalculateAgentCommission(parPrice);
end;</source>
        <Parameter name="parRetailSaleItem">
            <type name="RetailSaleItem"/>
        </Parameter>
        <Parameter name="parClient">
            <type name="Client"/>
        </Parameter>
        <Parameter name="parPrice">
            <type name="Decimal"/>
        </Parameter>
        <Parameter name="parTimeStamp">
            <type name="TimeStamp"/>
        </Parameter>
    </JadeMethod>
</Class>
