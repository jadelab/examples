<Class name="InitialDataLoader" id="70c2f2b7-95bb-4777-a1ee-99333e33de22">
    <superclass name="Object"/>
    <transient>true</transient>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <text>Utility class used to initialize the database</text>
    <DbClassMap file="eredef"/>
    <PrimAttribute name="zDirPath" id="57ecf2ff-2ce4-4dac-9832-608753c8b7f2">
        <length>301</length>
        <embedded>true</embedded>
        <type name="String"/>
        <access>protected</access>
    </PrimAttribute>
    <ImplicitInverseRef name="zCompany" id="fa3d196f-fce8-4a8c-ad5f-21dc4b114803">
        <embedded>true</embedded>
        <type name="Company"/>
        <access>protected</access>
    </ImplicitInverseRef>
    <JadeMethod name="loadData" id="6ea9e10f-8a15-4f05-a0ea-cafceebcb36f">
        <updating>true</updating>
        <source>loadData(directoryPath : String) updating;
// --------------------------------------------------------------------------------
// Method:		loadData
//
// Purpose:		Public method provided to initialize the database
//
// Parameters:	The path name of the directory containing the initial data files
// --------------------------------------------------------------------------------
vars
	startTime             : Integer;
	closeTendersStartTime : Integer;
	total                 : Integer;
	currentDate           : Date;		// Initialized to the current date

begin
	// Get the initial time
	startTime := app.clock;

	// Ensure the supplied directory path is valid
	self.zDirPath := self.zValidateDirectoryPath(directoryPath);

	// Purge all existing data. We simply delete the root Company object and let
	// JADE automatically delete all related objects via parent-child relationships.
	// However, beware of relying on cascading parent-child deletions when a very
	// large amount of data is involved as transactions can become too big (which
	// can then cause bottlenecks in the system).
	beginTransaction;
	delete Company.firstInstance;
	commitTransaction;

	beginTransaction;

	// Create our singleton persistent Company.
	// The Company constructor will initialize its properties.
	create self.zCompany persistent;

	// Load the rest of the data
	self.zLoadLocations;
	self.zLoadAgents;
	self.zLoadClients;
	self.zLoadCategories;
	self.zLoadRetailSaleItems;
	self.zLoadTenderSaleItems;
	self.zLoadCategoryRates;
	self.zLoadAgentRates;
	self.zLoadTenders;
	self.zLoadRetailSales;

	commitTransaction;

	// We must commit the first transaction before invoking the operation to
	// close tender sale items. This is because tender closures are done in a
	// server method, which requires its own transaction at the server (in
	// single user mode, this rule doesn't apply as the client and the server
	// are the same node). We must begin and commit the first transaction on
	// the client before we can begin the server transaction.

	closeTendersStartTime := app.clock;
	write "Closing tenders (server method)...";

	// Close tenders as at the date the database is being initialized (this
	// runs as a server method which begins and commits it own transaction)
	self.zCloseTendersAtCurrentDate(total);

	if total &gt; 0 then
		write total.String &amp; " tender sales built in " &amp; self.zGetElapsedTimeString(closeTendersStartTime);
	else
		write "No tenders to close as at " &amp; currentDate.String;
	endif;

epilog
	write "Database initialized in " &amp; ((app.clock - startTime) div MillisecondsPerSecond).String &amp; " seconds";
end;</source>
        <Parameter name="directoryPath">
            <type name="String"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="zCloseTendersAtCurrentDate" id="969700f5-40e2-443a-b446-b782e6c90157">
        <executionLocation>server</executionLocation>
        <source>zCloseTendersAtCurrentDate(total : Integer output) serverExecution, protected;
// --------------------------------------------------------------------------------
// Method:		zCloseTendersAtCurrentDate
//
// Purpose:		Closes tender sale items as at the current date, creating a tender
//              sale for the highest tender offer.
//              This method runs on the server, so it begins and commits its own
//              transaction.
//
// Parameters:  total - the total number of tender sale items closed is returned
//                      in this output parameter
// --------------------------------------------------------------------------------
vars
	currentDate : Date;    // Dates and times are initialized to the current date and time

begin
	beginTransaction;
	self.zCompany.closeTendersAtDate(currentDate, total);
	commitTransaction;
end;</source>
        <access>protected</access>
        <Parameter name="total">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="zGetElapsedTimeString" id="934a397e-d380-47c3-b895-e43064a58ee4">
        <source>zGetElapsedTimeString(startClock : Integer) : String protected;
// --------------------------------------------------------------------------------
// Method:		self.zGetElapsedTimeString
//
// Purpose:		Given a starting clock value, this method returns a string
//              representing the elapsed time in seconds
// --------------------------------------------------------------------------------
vars
	elapsedTime : Integer;

begin
	elapsedTime := (app.clock - startClock) div MillisecondsPerSecond;
	if elapsedTime &lt; 1 then
		return "less than 1 second";
	endif;
	return elapsedTime.String &amp; " seconds";
end;</source>
        <access>protected</access>
        <Parameter name="startClock">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="zGetNextToken" id="6e89a6fd-4157-43cf-ae31-76b30ef85d95">
        <source>zGetNextToken(str : String; pos : Integer io) : String protected;
// --------------------------------------------------------------------------------
// Method:		self.zGetNextToken
//
// Purpose:		Given a string and a starting position, this method returns the
//              next token from the string where tokens are separated by tabs.
//              If there are no more tokens left in the string, a null string is
//              returned. If the value of a token is "null", null is returned.
//
// Parameters:	str - the string from which the next token is to be extracted
//              pos - the position in str at which to start searching. pos is
//                    updated to be the position of the character immediately
//                    after the end of the token returned (ie: the starting
//                    position when searching for the next token).
// --------------------------------------------------------------------------------
vars
	len   : Integer;   // the length of the string
	idx   : Integer;   // the index of the current character in the string
	token : String;    // the token to be returned

begin
	len := str.length;

	if pos &lt; 1 then
		// If the start position is less than one, assume one
		pos := 1;
	elseif pos &gt; len then
		// If starting position is beyond the end of the string, return a null token
		return null;
	endif;

	// Skip leading tabs and spaces
	foreach idx in pos to len do
		if str[idx] &lt;&gt; Tab and str[idx] &lt;&gt; " " then
			break;
		endif;
	endforeach;

	if idx = len then
		// If the first non-blank character is the last character in the string,
		// then we just return it
		pos := len + 1;
		return str[len];
	elseif idx &gt; len then
		// The string is all white space, so no tokens to be found
		pos := idx;
		return null;
	endif;

	// Start with the first non-blank character
	pos := idx;

	// Look for a tab delimiter
	foreach idx in pos to len do
		if str[idx] = Tab then
			// Found a tab so stop looking			
			break;
		endif;
	endforeach;

	if idx &gt; len then
		// We hit the end of the string, so the token is all characters from
		// the start position to the end of the string
		token := str[pos : end].trimBlanks;
		pos := idx;
	else
		// Token is all characters from the start position up to, but not
		// including, the tab character
		token := str[pos : idx - pos].trimBlanks;
		pos := idx + 1;
	endif;

	if token = "null" then
		return null;
	endif;

	return token;
end;</source>
        <access>protected</access>
        <Parameter name="str">
            <type name="String"/>
        </Parameter>
        <Parameter name="pos">
            <usage>io</usage>
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="zLoadAgentRates" id="da7e081d-e036-4b79-864f-fc503d421628">
        <source>zLoadAgentRates() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadAgentRates
//
// Purpose:		Loads commission rates for agents from a text file
// --------------------------------------------------------------------------------
constants
	AgentRateFileName = "AgentRates.txt";

vars
	inputFile    : File;
	line         : String;
	agentName    : String;
	categoryName : String;
	ratePercent  : Decimal[5, 2];
	pos          : Integer;
	startClock   : Integer;
	category     : SaleItemCategory;
	agent        : Agent;
	count        : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; AgentRateFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are ignored
		if line &lt;&gt; null and line[1] &lt;&gt; "#" then
			pos := 1;

			agentName    := self.zGetNextToken(line, pos);
			categoryName := self.zGetNextToken(line, pos);
			ratePercent  := self.zGetNextToken(line, pos).Decimal;

			agent        := self.zCompany.allAgents[agentName];
			category     := self.zCompany.allSaleItemCategories[categoryName];

			agent.addCommissionRate(category.allCommissionRates[ratePercent]);

			write "    Loaded " &amp; agentName &amp; ", " &amp; categoryName &amp; ", " &amp; ratePercent.String;
			count := count + 1;
		endif;
	endwhile;

epilog
	delete inputFile;
	write count.String &amp; " agent rates loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadAgents" id="4e03ed2e-d466-4030-bae4-b2a9939c9984">
        <source>zLoadAgents() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadAgents
//
// Purpose:		Loads agents from a text file
// --------------------------------------------------------------------------------
constants
	AgentFileName = "Agents.txt";

vars
	inputFile  : File;
	line       : String;
	agentName  : String;
	address1   : String;
	address2   : String;
	address3   : String;
	email      : String;
	phone      : String;
	fax        : String;
	web        : String;
	pos        : Integer;
	startClock : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; AgentFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are ignored
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			agentName := self.zGetNextToken(line, pos);
			address1  := self.zGetNextToken(line, pos);
			address2  := self.zGetNextToken(line, pos);
			address3  := self.zGetNextToken(line, pos);
			email     := self.zGetNextToken(line, pos);
			phone     := self.zGetNextToken(line, pos);
			fax       := self.zGetNextToken(line, pos);
			web       := self.zGetNextToken(line, pos);

			self.zCompany.createAgent(agentName, address1, address2, address3, phone, fax, email, web);

			write "    Loaded " &amp; agentName &amp; ", " &amp; address1 &amp; ", " &amp; address2 &amp; ", " &amp; address3 &amp;
				", " &amp; email &amp; ", " &amp; phone &amp; ", " &amp; fax &amp; ", " &amp; web;
		endif;
	endwhile;

epilog
	delete inputFile;
	write self.zCompany.allAgents.size.String &amp; " agents loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadCategories" id="0ad35898-3cf7-4c1b-a31c-a8ddd7b493eb">
        <source>zLoadCategories() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadCategories
//
// Purpose:		Loads sale item categories from a text file
// --------------------------------------------------------------------------------
constants
	CategoryFileName = "Categories.txt";

vars
	inputFile    : File;
	line         : String;
	categoryName : String;
	description  : String;
	pos          : Integer;
	startClock   : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; CategoryFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are skipped
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			categoryName := self.zGetNextToken(line, pos);
			description  := self.zGetNextToken(line, pos);

			self.zCompany.createCategory(categoryName, description);

			write "    Loaded " &amp; categoryName &amp; ", " &amp; description;
		endif;
	endwhile;

epilog
	delete inputFile;
	write self.zCompany.allAgents.size.String &amp; " categories loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadCategoryRates" id="0b817a2e-923d-432f-a95e-cf2c421256ee">
        <source>zLoadCategoryRates() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadCategoryRates
//
// Purpose:		Loads sale item category commission rates from a text file
// --------------------------------------------------------------------------------
constants
	CategoryRateFileName = "CategoryRates.txt";

vars
	inputFile    : File;
	line         : String;
	categoryName : String;
	ratePercent  : Decimal[5, 2];
	pos          : Integer;
	startClock   : Integer;
	commRate     : CommissionRate;
	count        : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; CategoryRateFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are skipped
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			categoryName := self.zGetNextToken(line, pos);
			ratePercent  := self.zGetNextToken(line, pos).Decimal;

			create commRate persistent;
			commRate.updateForCreate(self.zCompany.allSaleItemCategories[categoryName], ratePercent);

			write "    Loaded " &amp; categoryName &amp; ", " &amp; ratePercent.String;
			count := count + 1;
		endif;
	endwhile;

epilog
	delete inputFile;
	write count.String &amp; " category rates loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadClients" id="f040febe-55ac-4188-b77d-5b9e3852c4d4">
        <source>zLoadClients() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadClients
//
// Purpose:		Loads clients from a text file
// --------------------------------------------------------------------------------
constants
	ClientFileName = "Clients.txt";

vars
	inputFile   : File;
	line        : String;
	clientName  : String;
	address1    : String;
	address2    : String;
	address3    : String;
	email       : String;
	phone       : String;
	fax         : String;
	web         : String;
	pos         : Integer;
	startClock  : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; ClientFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are skipped
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			clientName := self.zGetNextToken(line, pos);
			address1   := self.zGetNextToken(line, pos);
			address2   := self.zGetNextToken(line, pos);
			address3   := self.zGetNextToken(line, pos);
			email      := self.zGetNextToken(line, pos);
			phone      := self.zGetNextToken(line, pos);
			fax        := self.zGetNextToken(line, pos);
			web        := self.zGetNextToken(line, pos);

			self.zCompany.createClient(clientName, address1, address2, address3, phone, fax, email, web);

			write "    Loaded " &amp; clientName &amp; ", " &amp; address1 &amp; ", " &amp; address2 &amp; ", " &amp;
				address3 &amp; ", " &amp; email &amp; ", " &amp; phone &amp; ", " &amp; fax &amp; ", " &amp; web;
		endif;
	endwhile;

epilog
	delete inputFile;
	write self.zCompany.allClients.size.String &amp; " clients loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadLocations" id="ebcacaa3-9560-469f-a371-7191fa8c34d6">
        <source>zLoadLocations() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadLocations
//
// Purpose:		Loads countries and regions from a text file
// --------------------------------------------------------------------------------
constants
	LocationFileName = "Locations.txt";

vars
	inputFile   : File;
	line        : String;
	countryName : String;
	regionName  : String;
	country     : Country;
	pos         : Integer;
	startClock  : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; LocationFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are skipped
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			countryName := self.zGetNextToken(line, pos);
			regionName  := self.zGetNextToken(line, pos);
			country     := self.zCompany.allCountries[countryName];

			if country = null then
				country := self.zCompany.createCountry(countryName);
			endif;
			country.createRegion(regionName);

			write "    Loaded " &amp; countryName &amp; ", " &amp; regionName;
		endif;
	endwhile;

epilog
	delete inputFile;
	write self.zCompany.getNumberOfLocations.String &amp; " locations loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadRetailSaleItems" id="3ab35ea3-15d5-430d-b7b8-e12873734061">
        <source>zLoadRetailSaleItems() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadRetailSaleItems
//
// Purpose:		Loads retail sale items from a text file
// --------------------------------------------------------------------------------
constants
	RetailItemFileName = "RetailItems.txt";

vars
	inputFile     : File;
	line          : String;
	code          : String;
	codePrefix    : String;
	codeNumber    : Integer;
	shortDesc     : String;
	fullDesc      : String;
	photoFileName : String;
	forSaleDate   : Date;
	agentName     : String;
	countryName   : String;
	regionName    : String;
	categoryName  : String;
	price         : Decimal[12, 2];
	retailItem    : RetailSaleItem;
	pos           : Integer;
	startClock    : Integer;
	totalItems    : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; RetailItemFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are ignored
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			code := self.zGetNextToken(line, pos);
			code.getCodePrefixAndNumber(codePrefix, codeNumber);

			shortDesc     := self.zGetNextToken(line, pos);
			fullDesc      := self.zGetNextToken(line, pos);
			photoFileName := self.zDirPath &amp; self.zGetNextToken(line, pos);
			price         := self.zGetNextToken(line, pos).Decimal;
			forSaleDate   := self.zGetNextToken(line, pos).Date;
			agentName     := self.zGetNextToken(line, pos);
			countryName   := self.zGetNextToken(line, pos);
			regionName    := self.zGetNextToken(line, pos);
			categoryName  := self.zGetNextToken(line, pos);

			create retailItem persistent;
			retailItem.updateForCreate(codePrefix,
			                        codeNumber,
			                        shortDesc,
			                        fullDesc,
			                        forSaleDate,
			                        app.loadPicture(photoFileName),
			                        self.zCompany.allAgents[agentName],
			                        self.zCompany.getRegion(countryName, regionName),
				                    self.zCompany.allSaleItemCategories[categoryName],
				                    price,
				                    self.zCompany);
			totalItems := totalItems + 1;

			write "    Loaded " &amp; code &amp; ", " &amp; shortDesc &amp; ", " &amp; fullDesc &amp; ", " &amp; photoFileName &amp;
			    ", " &amp; price.String &amp; ", " &amp; forSaleDate.String &amp; ", " &amp; agentName &amp; ", " &amp;
			    countryName &amp; ", " &amp; regionName &amp; ", " &amp; categoryName;
		endif;
	endwhile;

epilog
	delete inputFile;
	write totalItems.String &amp; " retail sale items loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadRetailSales" id="1329bed9-2a3e-4e17-9003-77ac19cda4b4">
        <source>zLoadRetailSales() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadRetailSales
//
// Purpose:		Loads retail sales from a text file
// --------------------------------------------------------------------------------
constants
	RetailSaleFileName = "RetailSales.txt";

vars
	inputFile  : File;
	line       : String;
	price      : Decimal[12, 2];
	saleDate   : Date;
	saleTime   : Time;
	clientName : String;
	itemCode   : String;
	codePrefix : String;
	codeNumber : Integer;
	pos        : Integer;
	startClock : Integer;
	totalSales : Integer;
	timeStamp  : TimeStamp;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; RetailSaleFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are ignored
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			timeStamp.setDate(self.zGetNextToken(line, pos).Date);
			timeStamp.setTime(self.zGetNextToken(line, pos).Time);
			price      := self.zGetNextToken(line, pos).Decimal;
			clientName := self.zGetNextToken(line, pos);
			itemCode   := self.zGetNextToken(line, pos);
			itemCode.getCodePrefixAndNumber(codePrefix, codeNumber);

			self.zCompany.createRetailSale(self.zCompany.allSaleItems[codePrefix, codeNumber].RetailSaleItem,
				self.zCompany.allClients[clientName], price, timeStamp);

			totalSales := totalSales + 1;
			write "    Loaded " &amp; saleDate.String &amp; ", " &amp; saleTime.String &amp; ", " &amp;
				price.String &amp; ", " &amp; clientName &amp; ", " &amp; itemCode;
		endif;
	endwhile;

epilog
	delete inputFile;
	write totalSales.String &amp; " retail sales loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadTenderSaleItems" id="098e5265-aacd-402f-b5ca-af9ccadf9f5e">
        <source>zLoadTenderSaleItems() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadTenderSaleItems
//
// Purpose:		Loads tender sale items from a text file
// --------------------------------------------------------------------------------
constants
	TenderItemFileName = "TenderItems.txt";

vars
	inputFile     : File;
	line          : String;
	code          : String;
	codePrefix    : String;
	codeNumber    : Integer;
	shortDesc     : String;
	fullDesc      : String;
	photoFileName : String;
	agentName     : String;
	countryName   : String;
	regionName    : String;
	categoryName  : String;
	minimumPrice  : Decimal[12, 2];
	forSaleDate   : Date;
	closureDate   : Date;
	tenderItem    : TenderSaleItem;
	pos           : Integer;
	startClock    : Integer;
	totalItems    : Integer;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; TenderItemFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are skipped
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			code          := self.zGetNextToken(line, pos);
			code.getCodePrefixAndNumber(codePrefix, codeNumber);
			shortDesc     := self.zGetNextToken(line, pos);
			fullDesc      := self.zGetNextToken(line, pos);
			photoFileName := self.zDirPath &amp; self.zGetNextToken(line, pos);
			minimumPrice  := self.zGetNextToken(line, pos).Decimal;
			forSaleDate   := self.zGetNextToken(line, pos).Date;
			closureDate   := self.zGetNextToken(line, pos).Date;
			agentName     := self.zGetNextToken(line, pos);
			countryName   := self.zGetNextToken(line, pos);
			regionName    := self.zGetNextToken(line, pos);
			categoryName  := self.zGetNextToken(line, pos);

			create tenderItem persistent;
			tenderItem.updateForCreate(codePrefix,
			                        codeNumber,
			                        shortDesc,
			                        fullDesc,
			                        forSaleDate,
			                        app.loadPicture(photoFileName),
				                    self.zCompany.allAgents[agentName],
				                    self.zCompany.getRegion(countryName, regionName),
				                    self.zCompany.allSaleItemCategories[categoryName],
				                    closureDate,
				                    minimumPrice,
				                    self.zCompany);

			totalItems := totalItems + 1;
			write "    Loaded " &amp; code &amp; ", " &amp; shortDesc &amp; ", " &amp; fullDesc &amp; ", " &amp; photoFileName &amp;
			    ", " &amp; minimumPrice.String &amp; ", " &amp; forSaleDate.String &amp; ", " &amp; closureDate.String &amp;
				", " &amp; agentName &amp; ", " &amp; countryName &amp; ", " &amp; regionName &amp; ", " &amp; categoryName;
		endif;
	endwhile;

epilog
	delete inputFile;
	write totalItems.String &amp; " tender sale items loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zLoadTenders" id="82ade893-0687-4b57-acb9-f1c841537c70">
        <source>zLoadTenders() protected;
// --------------------------------------------------------------------------------
// Method:		zLoadTenders
//
// Purpose:		Loads tenders from a text file
// --------------------------------------------------------------------------------
constants
	TenderFileName = "Tenders.txt";

vars
	inputFile      : File;
	line           : String;
	offerPrice     : Decimal[12, 2];
	offerDate      : Date;
	offerTime      : Time;
	clientName     : String;
	itemCode       : String;
	codePrefix     : String;
	codeNumber     : Integer;
	pos            : Integer;
	startClock     : Integer;
	tender         : Tender;
	totalTenders   : Integer;
	timeStamp      : TimeStamp;

begin
	startClock := app.clock;

	create inputFile transient;
	inputFile.allowCreate := false;
	inputFile.allowReplace := false;
	inputFile.kind := File.Kind_ANSI;	//if loading in a unicode system, will be automatically converted
	inputFile.openInput(self.zDirPath &amp; TenderFileName);

	write "Loading " &amp; inputFile.fileName &amp; "...";

	while not inputFile.endOfFile do
		line := inputFile.readLine.trimBlanks;

		// Empty lines and lines beginning with # are ignored
		if line &lt;&gt; "" and line[1] &lt;&gt; "#" then
			pos := 1;

			offerPrice     := self.zGetNextToken(line, pos).Decimal;
			timeStamp.setDate(self.zGetNextToken(line, pos).Date);
			timeStamp.setTime(self.zGetNextToken(line, pos).Time);
			clientName     := self.zGetNextToken(line, pos);
			itemCode       := self.zGetNextToken(line, pos);
			itemCode.getCodePrefixAndNumber(codePrefix, codeNumber);

			create tender persistent;
			tender.updateForCreate(offerPrice, timeStamp, self.zCompany.allClients[clientName],
				self.zCompany.allSaleItems[codePrefix, codeNumber].TenderSaleItem);

			totalTenders := totalTenders + 1;
			write "    Loaded " &amp; offerPrice.String &amp; ", " &amp; offerDate.String &amp; ", " &amp;
			    offerTime.String &amp; ", " &amp; clientName &amp; ", " &amp; itemCode;
		endif;
	endwhile;

epilog
	delete inputFile;
	write totalTenders.String &amp; " tenders loaded in " &amp; self.zGetElapsedTimeString(startClock);
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="zValidateDirectoryPath" id="4b3ea717-c6f2-4492-a807-586137b30425">
        <source>zValidateDirectoryPath(directoryPath : String) : String protected;
// --------------------------------------------------------------------------------
// Method:		zValidateDirectoryPath
//
// Purpose:		Validates a directory path. Raises an InvalidDirectoryPath
//              ModelException if the path is not valid.
//
// Returns:     The directory path with a "/" suffix
// --------------------------------------------------------------------------------
vars
	returnDirPath : String;
	dir           : FileFolder;

begin
	returnDirPath := directoryPath.trimBlanks;

	if returnDirPath.length &lt; 1 then
		app.raiseModelException(InvalidDirectoryPath);
		return null;
	endif;

	if returnDirPath[returnDirPath.length] &lt;&gt; "/" and returnDirPath[returnDirPath.length] &lt;&gt; "\" then
		returnDirPath := returnDirPath &amp; "/";
	endif;

	create dir transient;

	if not dir.isValidPathName(returnDirPath) then
		app.raiseModelException(InvalidDirectoryPath);
		return null;
	endif;

	dir.fileName := returnDirPath;
	if not dir.isAvailable then
		app.raiseModelException(InvalidDirectoryPath);
		return null;
	endif;

	return returnDirPath;

epilog
	delete dir; // does nothing if dir is null
end;</source>
        <access>protected</access>
        <Parameter name="directoryPath">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
</Class>
