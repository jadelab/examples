<Class name="GCommonSchema" id="5d879223-c772-45dc-8457-b796f1258f2c">
    <superclass name="RootSchemaGlobal"/>
    <transient>true</transient>
    <sharedTransientAllowed>true</sharedTransientAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <DbClassMap file="common"/>
    <JadeMethod name="commonDeadlockExceptionHandler" id="056721f1-56bd-4285-9788-d4638acbacf7">
        <source>commonDeadlockExceptionHandler(exObj : DeadlockException) : Integer;
// --------------------------------------------------------------------------------
// Method:		commonDeadlockExceptionHandler
//
// Purpose:		Generic exception handler to abort operation on deadlock exceptions
//
// Parameters:	Deadlock exception object
// Returns:		Ex_Abort_Action
// --------------------------------------------------------------------------------
vars
	logFileName : String;

begin
	// Log the exception details
	logFileName := self.zLogException(exObj);

	// Abort the transaction (does nothing if not in transaction state)
	abortTransaction;

	// Tell the user about the exception
	if currentSession = null then
		// We're not running over the web
		app.msgBox($CmnDeadlockBody(logFileName), $CmnDeadlockHeading, MsgBox_OK_Only);
	else
		// We're running over the web
		app.msgBox($CmnDeadlockBodyWeb(logFileName), $CmnDeadlockHeading, MsgBox_OK_Only);
	endif;

	// Abort the operation
	return Ex_Abort_Action;
end;</source>
        <Parameter name="exObj">
            <type name="DeadlockException"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="commonExceptionHandler" id="bda36195-eeac-4997-996c-e9f3377a4d67">
        <source>commonExceptionHandler(exObj : Exception) : Integer;
// --------------------------------------------------------------------------------
// Method:		commonExceptionHandler
//
// Purpose:		Example of a simple generic, catch-all exception handler
//
// Parameters:	Exception object
// Returns:		Ex_Pass_Back or Ex_Abort_Action
// --------------------------------------------------------------------------------

constants
	CONNECTION_ERROR_MIN	: Integer = 30000;
	CONNECTION_ERROR_MAX	: Integer = 32999;
	
vars
	errItem, logFileName : String;

begin
	// Log the exception details
	logFileName := self.zLogException(exObj);

	errItem := exObj.errorItem.trimBlanks;
	if errItem = null then
		errItem := "Not available";
	endif;
	
	// any exception from Jade code when running the Java application is just logged
	if app.applicationType = app.ApplicationType_Non_GUI then
		// Abort the operation. Abort the current transaction before returning.
		// abortTransaction doesn't give an error if we're not in transaction state.
		abortTransaction;
		return Ex_Abort_Action;
	endif;

	// It is important that all connection errors for web applications are passed
	// back to web framework. This allows the framework to close and reopen the
	// connection and send an appropriate response.
	if  app.applicationType = app.ApplicationType_Non_GUI_Web or 
		app.applicationType = app.ApplicationType_Web_Enabled then
		
		//check for a connection style exception (errorCode ranges from 30000 - 32999).
		if exObj.errorCode &gt;= CONNECTION_ERROR_MIN and 
		   exObj.errorCode &lt;= CONNECTION_ERROR_MAX then
			// We're running on the web. Abort the current transaction. This
			// will not give an error if not in transaction state, and it
			// will release all transaction duration locks.
		   abortTransaction;
		   
			// do not handle this exception in user code - pass back to the web framework
		   return Ex_Pass_Back;
		endif;
	endif;	

	// Tell the user about the exception
	if currentSession = null then
		// We're not running over the web so give the user a more interactive message
		// box that allows them to abort or pass back the exception
		if app.msgBox($CmnExceptionBody(
				exObj.errorCode.String, exObj.text, errItem, logFileName),
				$CmnExceptionHeading(exObj.errorCode.String),
				MsgBox_Retry_Cancel + MsgBox_Stop_Icon) = MsgBox_Return_Retry then
	    	// If they select Retry, they want to pass the exception back to the next
	    	// handler in the stack. If there are no exception handlers armed, control
	    	// will be passed to the default JADE exception handler. This allows the
	    	// user to open the debug window.
	    	return Ex_Pass_Back;
	    endif;
	else
		// We're running over the web so give a simpler message and always abort
		app.msgBox($CmnExceptionBodyWeb(
				exObj.errorCode.String, exObj.text, errItem, logFileName),
				$CmnExceptionHeading(exObj.errorCode.String),
				MsgBox_OK_Only + MsgBox_Stop_Icon);
	endif;

	// Abort the operation. Abort the current transaction before returning.
	// abortTransaction doesn't give an error if we're not in transaction state.
	abortTransaction;
	return Ex_Abort_Action;
end;</source>
        <Parameter name="exObj">
            <type name="Exception"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="commonLockExceptionHandler" id="faf8f7a7-cddc-40f0-8c98-ef167e8f5029">
        <source>commonLockExceptionHandler(lockExObj : LockException input) : Integer;
// --------------------------------------------------------------------------------
// Method:		commonLockExceptionHandler
//
// Purpose:		Example of a simple generic lock exception handler
//
// Parameters:	LockException object
// Returns:		Ex_Continue or Ex_Abort_Action
// --------------------------------------------------------------------------------
begin
	// If the user retries and gets the lock then we can continue
	if lockExceptionHandlerMsgBox(lockExObj) then
		return Ex_Continue;
	endif;

	// They want to abort the operation. Abort the current transaction before returning.
	// abortTransaction doesn't give an error if we're not in transaction state.
	abortTransaction;
	return Ex_Abort_Action;
end;</source>
        <Parameter name="lockExObj">
            <usage>input</usage>
            <type name="LockException"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="confirmDelete" id="15bdbdc3-b38e-4772-a1e9-a3f3dcc88675">
        <source>confirmDelete(itemToDelete : String) : Boolean;
// --------------------------------------------------------------------------------
// Method:		confirmDelete
//
// Purpose:		Gets confirmation from the user for a delete request
//
// Parameters:	itemToDelete - A description of the item to delete
// Returns:		True if the user confirms the request, otherwise false
// --------------------------------------------------------------------------------
begin
	return (app.msgBox($CmnDialogDeleteMessage &amp; itemToDelete &amp;
			"." &amp; CrLf &amp; CrLf &amp; $CmnConfirmDelete, $CmnDelete,
			MsgBox_Question_Mark_Icon + MsgBox_Yes_No) = MsgBox_Return_Yes);
end;</source>
        <Parameter name="itemToDelete">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="lockExceptionHandlerMsgBox" id="f7cd5b15-b373-42a5-9301-f9dca5ac2e17">
        <source>lockExceptionHandlerMsgBox(lockExObj : LockException) : Boolean;
// --------------------------------------------------------------------------------
// Method:		lockExceptionHandlerMsgBox
//
// Purpose:		If we're not running on the web, display a message box allowing the
//              user to retry a lock. If we're running on the web, just display a
//              general error message and return.
//
// Parameters:	LockException object
// Returns:		True if we obtain the lock, otherwise false
// --------------------------------------------------------------------------------
vars
	lockedByUser : String;
	retries      : Integer;

begin
	retries := 0;

	// Build a string representing the user/process that holds the lock
	if lockExObj.targetLockedBy.userCode &lt;&gt; null then
		lockedByUser:= $CmnLockMessageUser &amp; lockExObj.targetLockedBy.userCode;
	else
		lockedByUser:= $CmnLockMessageUnknownUser;
	endif;

	if currentSession &lt;&gt; null then
		// We're running over the web so we don't want to put up an interactive
		// message or allow the user to retry. Just give a general error message
		// and return false.
		app.msgBox($CmnLockMessageBodyWeb(getOidStringForObject(lockExObj.lockTarget),
				lockedByUser, retries.String), $CmnLockMessageHeading, MsgBox_OK_Only);
        return false;
    endif;

	while true do
		// Prompt the user
		if app.msgBox($CmnLockMessageBody(getOidStringForObject(lockExObj.lockTarget),
				lockedByUser, retries.String), $CmnLockMessageHeading,
				MsgBox_Retry_Cancel + MsgBox_Exclamation_Mark_Icon) = MsgBox_Return_Retry then
        	// We're to retry the lock
			retries := retries + 1;
			// tryLock attempts to lock an object but returns a boolean indicating success or
			// failure instead of raising an exception
			if tryLock(lockExObj.lockTarget, lockExObj.lockType, lockExObj.lockDuration, LockTimeout_Immediate) then
				// We got the lock
				return true;
			endif;
		else
			// Had enough of retrying
			break;
		endif;
	endwhile;

	return false;
end;</source>
        <Parameter name="lockExObj">
            <type name="LockException"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="zLogException" id="13ef3846-aa09-4071-b5f0-855bbf7b2de3">
        <source>zLogException(exObj : Exception) : String protected;
// --------------------------------------------------------------------------------
// Method:		zLogException
//
// Purpose:		Log exception details to a file
//
// Parameters:	Exception object
// Returns:		The name of the log file
// --------------------------------------------------------------------------------
vars
	logFile : File;
	folder  : FileFolder;

begin
	// Create an exceptions directory in our install directory if it doesn't exist
	create folder transient;
	folder.usePresentationFileSystem := false;
	folder.allowCreate := true;
	folder.fileName := app.getJadeInstallDirAppServer &amp; "/../logs/erewhon";
	if not folder.isAvailable then
		folder.make;
	endif;

	// Create a file object for our current application's exception log
	create logFile transient;
	logFile.usePresentationFileSystem := false;
	logFile.allowCreate := true;
	logFile.allowReplace := false;
	logFile.mode := File.Mode_Append;
	logFile.shareMode := File.Share_ReadWrite;
	logFile.fileName := folder.fileName &amp; "/" &amp; app.name &amp; ".log";

	// Log the current time, and our computer and user names
	logFile.open;
	logFile.writeLine(app.actualTime.String &amp; " " &amp; app.computerName &amp; " " &amp; app.userName);
	logFile.close;

	// Log the exception details and process stack. This is the same information JADE
	// logs in the default exception handler.
	exObj.logSelf(logFile.fileName);
	exObj.logProcessHistory(logFile.fileName);

	logFile.open;
	logFile.writeLine("--------------------------------------------------------------------------------");
	logFile.close;

	// Return the name of the log file
	return logFile.fileName;

epilog
	delete folder;		// Does nothing if folder is null
	delete logFile;		// Does nothing if logFile is null
end;</source>
        <access>protected</access>
        <Parameter name="exObj">
            <type name="Exception"/>
        </Parameter>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </JadeMethod>
</Class>
