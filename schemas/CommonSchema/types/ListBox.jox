<GUIClass name="ListBox">
    <JadeMethod name="findParent" id="78a14b62-f139-4819-8e5d-aa48961f7e65">
        <source>findParent(childIndex:Integer):Integer;
// --------------------------------------------------------------------------------
// Method:		findParent
//
// Purpose:		Finds the index of an item's parent in an hierarchical list.
//
// Parameters:	childIndex (Integer) - The index of the child item.
// Returns:		Integer - The index of the child item's parent item.
// --------------------------------------------------------------------------------
/*
	This method will search backwards through the listbox,
	an return the parent list item in the list hierarchy.

	The index position of the parent item will be returned.

	If the starting index position (childIndex) is 0 or 1,
	then zero will be returned.  Likewise, if a parent is
	not found, then a zero will also be returned.
*/
vars
	itemindex	: Integer;	// Current list item index.
	level		: Integer;	// Current list item level.
	childLevel	: Integer;	// Item Level of the child list item.

begin
	if childIndex = 0 or childIndex = 1 or self.listCount = 0 then
		return 0;
	endif;

	childLevel	:= self.itemLevel[childIndex];
	itemindex	:= childIndex - 1;

	while itemindex &gt; 0 do
		if level &lt;= self.listCount then
			level := self.itemLevel[itemindex];

			if level = (childLevel - 1) then
				// The parent item level should be 1 less than that of it's child.
				return itemindex;
			endif;
		endif;

		itemindex := itemindex - 1;	// We're stepping backwards through the list.
	endwhile;

	return 0;
end;</source>
        <Parameter name="childIndex">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </JadeMethod>
</GUIClass>
