This repository is for demonstration purposes only to showcase the capabilities of the [jadegit](https://gitlab.com/jadelab/jadegit) project.

The example schemas within are [provided by JADE](https://www.jadeworld.com/pdf/white-papers/examples/2018/JADEExamples.exe), which have been loaded/converted into the extract format used by jadegit.